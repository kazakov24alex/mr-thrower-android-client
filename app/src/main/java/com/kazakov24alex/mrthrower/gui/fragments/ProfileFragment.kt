package com.kazakov24alex.mrthrower.gui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kazakov24alex.mrthrower.R
import kotlinx.android.synthetic.main.fragment_profile.*
import library.codes.OsCodes


class ProfileFragment : Fragment() {

    private val osImages = intArrayOf(
            R.drawable.windows_black_512,
            R.drawable.android_black_512
    )

    var contactName: String = "CONTACT_NAME"
        set(value) { field = value; viewProfileContactName.text = value }
    var deviceName: String = "DEVICE_NAME"
        set(value) { field = value; viewProfileDeviceName.text = value }
    var os: Byte = 0
        set(value) {
            field = value;
            when (value) {
                OsCodes.WINDOWS -> viewProfileOsImage.setImageResource(osImages[0])
                OsCodes.ANDROID -> viewProfileOsImage.setImageResource(osImages[1])
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_profile, container, false)
    }


}

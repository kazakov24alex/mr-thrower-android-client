package com.kazakov24alex.mrthrower.gui.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.kazakov24alex.mrthrower.R
import com.kazakov24alex.mrthrower.database.DbManager
import com.kazakov24alex.mrthrower.utils.experts.BluetoothExpert

import com.synnapps.carouselview.ViewListener
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.carousel_view.view.*
import library.codes.ThrowModeCodes


class WelcomeActivity : AppCompatActivity() {

    companion object {
        private const val MODE_BLUETOOTH    = 0
        private const val MODE_WIFI_NETWORK = 1
        private const val MODE_WIFI_DIRECT  = 2

        const val INTENT_EXTRA_MODE = "MODE_ID"
    }


    lateinit private var backgroundColors: IntArray
    lateinit private var modeImages: IntArray
    lateinit private var modeTitles: Array<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        // Resources initialization"#ffa500
        backgroundColors = intArrayOf(
                R.color.WHITE,
                R.color.WHITE,
                R.color.WHITE
        )
        modeImages = intArrayOf(
                R.drawable.bt_welcome,
                R.drawable.wifi_welcome,
                R.drawable.direct_welcome)
        modeTitles = arrayOf(
                resources.getString(R.string.mode_bluetooth).toUpperCase(),
                resources.getString(R.string.mode_wifi_network).toUpperCase(),
                resources.getString(R.string.mode_wifi_direct).toUpperCase())

        // Carousel initialization
        viewWelcomeCarousel.pageCount = modeImages.size
        viewWelcomeCarousel.slideInterval = 10000
        viewWelcomeCarousel.setViewListener(viewListener)

        // TODO test
        DbManager.openDatabase()
    }


    private var viewListener: ViewListener = ViewListener { position ->
        val carouselView = layoutInflater.inflate(R.layout.carousel_view, null)

        val layout = carouselView.carousel_layout
        val modeImageView = carouselView.carousel_image
        val modeLabelTextView = carouselView.carousel_label

        layout.setBackgroundColor(resources.getColor(backgroundColors[position]))
        modeImageView.setImageResource(modeImages[position])
        modeLabelTextView.text = modeTitles[position]

        modeImageView.setOnClickListener() {
            when(position) {
                MODE_WIFI_NETWORK -> {
                    val intent = Intent(this, ContactsActivity::class.java)
                    intent.putExtra(INTENT_EXTRA_MODE, ThrowModeCodes.WIFI_NETWORK)
                    startActivity(intent)
                }
                MODE_BLUETOOTH   ->  {
                    if(!BluetoothExpert.isAvailable()) {
                        Toast.makeText(this, resources.getString(R.string.welcome_bluetooth_unavailable), Toast.LENGTH_SHORT).show()
                    } else {
                        val intent = Intent(this, ContactsActivity::class.java)
                        intent.putExtra(INTENT_EXTRA_MODE, ThrowModeCodes.BLUETOOTH)
                        startActivity(intent)
                    }
                }
                MODE_WIFI_DIRECT ->  {
                    val intent = Intent(this, ContactsActivity::class.java)
                    intent.putExtra(INTENT_EXTRA_MODE, ThrowModeCodes.WIFI_DIRECT)
                    startActivity(intent)
                }
            }

        }
        carouselView
    }

}

package com.kazakov24alex.mrthrower.gui.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.kazakov24alex.mrthrower.R
import com.kazakov24alex.mrthrower.database.DbManager
import com.kazakov24alex.mrthrower.utils.wrappers.ContactWrapper
import library.objects.GreetingObject
import library.wrappers.MacAddress


class ContactsAdapter(
        private var activity: Activity,
        private var contacts: ArrayList<ContactWrapper>): BaseAdapter() {

    companion object {
        const val OS_WINDOWS = 0
        const val OS_ANDROID = 1
    }


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val osImages = intArrayOf(
            R.drawable.windows_black_512,
            R.drawable.android_black_512
    )


//======================================================================================================================
// ACTIONS
//======================================================================================================================

    fun addContact(greeting: GreetingObject) {
        var filterList: List<GreetingObject> = contacts.filter { s -> s.macAddress.compareTo(greeting.macAddress) == 0 }
        if(filterList.size == 0) {
            val contact = ContactWrapper(greeting)
            val num = DbManager.selectUnreadMessageByMac(contact.macAddress)
            contact.setUnreadCounter(num)

            contacts.add(contact)
            notifyDataSetChanged()
        }
    }

    fun clearContactsList() {
        contacts.clear()
        notifyDataSetChanged()
    }

    fun removeContact(macAddress: MacAddress) {
        contacts.remove(contacts.filter { s -> s.macAddress.compareTo(macAddress) == 0 }.single())
        notifyDataSetChanged()
    }

    fun incrementMesCounter(macAddress: MacAddress) {
        contacts.filter { s -> s.macAddress.compareTo(macAddress) == 0 }.single().incrementUnreadCounter()
        notifyDataSetChanged()
    }

    fun clearMesCounter(macAddress: MacAddress) {
        contacts.filter { s -> s.macAddress.compareTo(macAddress) == 0 }.single().nullifyCounter()
        notifyDataSetChanged()
    }

    fun getSize(): Int {
        return contacts.size
    }

//======================================================================================================================
// ADAPTER OVERRIDE
//======================================================================================================================

    private class ViewHolder(row: View?) {
        var name: TextView? = null
        var deviceName: TextView? = null
        var osImage: ImageView? = null
        var mesCounter: TextView? = null

        init {
            this.name = row?.findViewById<TextView>(R.id.viewContactName)
            this.deviceName = row?.findViewById<TextView>(R.id.viewContactDeviceName)
            this.osImage = row?.findViewById<ImageView>(R.id.viewContactOsImage)
            this.mesCounter = row?.findViewById<TextView>(R.id.viewContactCounter)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.fragment_contact, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        val contact = contacts[position]
        viewHolder.name?.text = contact.name
        viewHolder.deviceName?.text = contact.device
        viewHolder.osImage?.setImageResource(osImages[contact.os-1])
        viewHolder.mesCounter?.text = contact.getCounterValue().toString()

        if(contact.getCounterValue() == 0)
            viewHolder.mesCounter?.visibility = View.INVISIBLE
        else
            viewHolder.mesCounter?.visibility = View.VISIBLE

        return view as View
    }

    override fun getItem(i: Int): GreetingObject {
        return contacts[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getCount(): Int {
        return contacts.size
    }
}

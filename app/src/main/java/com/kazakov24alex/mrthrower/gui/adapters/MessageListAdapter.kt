package com.kazakov24alex.mrthrower.gui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.kazakov24alex.mrthrower.R
import android.view.LayoutInflater
import android.view.ViewGroup
import com.kazakov24alex.mrthrower.database.DbManager
import com.kazakov24alex.mrthrower.gui.activities.ChatActivity
import com.kazakov24alex.mrthrower.utils.wrappers.MessageWrapper
import com.kazakov24alex.mrthrower.utils.wrappers.ThrowWrapper
import com.kazakov24alex.mrthrower.utils.experts.Utils
import com.kazakov24alex.mrthrower.service.AppService
import kotlinx.android.synthetic.main.item_chat_file_incoming.view.*
import kotlinx.android.synthetic.main.item_chat_file_outcoming.view.*
import kotlinx.android.synthetic.main.item_chat_message_incoming.view.*
import kotlinx.android.synthetic.main.item_chat_message_outcoming.view.*
import library.codes.*
import library.objects.*
import library.wrappers.MacAddress
import java.util.*
import java.util.logging.Logger
import android.content.ClipData
import android.content.ClipboardManager
import android.widget.Toast
import android.content.Intent
import android.net.Uri
import android.support.v4.app.ActivityCompat.startActivityForResult
import java.io.File
import android.content.ActivityNotFoundException
import android.support.v4.content.ContextCompat.startActivity
import android.webkit.MimeTypeMap
import android.content.ContentResolver






class MessageListAdapter(
        private val ctx: Context,
        private val MODE: Byte,
        private val mMacContact: MacAddress,
        private val mChatVector: Vector<BaseProtocolObject>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        val log = Logger.getLogger(AppService::class.java.name)

        private const val VIEW_TYPE_MESSAGE_SENT = 1
        private const val VIEW_TYPE_MESSAGE_RECEIVED = 2
        private const val VIEW_TYPE_FILE_SENT = 3
        private const val VIEW_TYPE_FILE_RECEIVED = 4

    }

    private var selectedMessages = 0
    private var selectedThrows = 0


//======================================================================================================================
// DATABASE ACTIONS
//======================================================================================================================


    // yes, i khow, that it is crutch TODO remake
    fun <T> compareBy(vararg selectors: (BaseProtocolObject) -> Comparable<*>?): Comparator<BaseProtocolObject> {
        return object : Comparator<BaseProtocolObject> {
            override fun compare(a: BaseProtocolObject, b: BaseProtocolObject): Int {
                var timeID1: Long = 0
                var timeID2: Long = 0

                if(a.code == PacketCodes.MESSAGE)
                    timeID1 = (a as MessageWrapper).timeID.time
                else
                    timeID1 = (a as ThrowWrapper).timeID.time

                if(b.code == PacketCodes.MESSAGE)
                    timeID2 = (b as MessageWrapper).timeID.time
                else
                    timeID2 = (b as ThrowWrapper).timeID.time


                if(timeID1 > timeID2) return 1
                else return -1
            }
        }
    }


    fun loadFromDatabase() {
        val vector: Vector<BaseProtocolObject> = Vector()

        val messages = DbManager.selectMessagesByMac(mMacContact)
        for(mes in messages) {
            // mark as read
            DbManager.updateMessage(mes.address, mes.timeID, ThrowStateCodes.STATE_FINISHED)
            vector.add(mes)
        }

        val throws = DbManager.selectThrowsByMac(mMacContact)
        for(tFile in throws) {
            vector.add(tFile)
        }

        val sortedVector = vector.sortedWith(compareBy<BaseProtocolObject>({it.code}))

        mChatVector.addAll(sortedVector)
        notifyDataSetChanged()
    }



//======================================================================================================================
// UPDATE RECYCLER VIEW
//======================================================================================================================

    fun addMessage(message: MessageObject) {
        var direction = DirectionCodes.OUTCOMING
        if(message.address.compareTo(mMacContact) == 0)
            direction = DirectionCodes.INCOMING

        val mes = MessageWrapper(message, direction, MODE)
        mChatVector.add(mes)
        notifyDataSetChanged()
    }

    fun addThrowRequest(request: ThrowRequestObject) {
        var direction = DirectionCodes.OUTCOMING
        if(request.address.compareTo(mMacContact) == 0)
            direction = DirectionCodes.INCOMING

        // add all files from request to chat recycler view
        for(tFile in request.throwFiles) {
            val file = ThrowWrapper(tFile, direction, MODE, ThrowTypeCodes.FILE)
            file.setState(ThrowStateCodes.STATE_REQUEST)

            mChatVector.add(file)
        }

        notifyDataSetChanged()
    }

    fun addThrowResponse(response: ThrowResponseObject) {
        // find need file and update status from response
        for(message in mChatVector) {
            if(message.code != PacketCodes.MESSAGE) {
                val tFile = message as ThrowWrapper
                // check TimeID
                if(response.timeID.compareTo(tFile.timeID) == 0) {
                    if(response.isNegative) {
                        tFile.setState(ThrowStateCodes.STATE_REJECTED)
                    } else {
                        tFile.setState(ThrowStateCodes.STATE_PROCESS)
                    }

                    notifyDataSetChanged()
                    return
                }
            }
        }
    }

    fun addThrowProgress(progress: ThrowProgressObject) {
        // find need file and update progress from ThrowProgress
        for(message in mChatVector) {
            if(message.code != PacketCodes.MESSAGE) {
                val tFile = message as ThrowWrapper
                if(progress.timeID.compareTo(tFile.timeID) == 0) {
                    tFile.setState(ThrowStateCodes.STATE_PROCESS)
                    tFile.setCurrentSize(progress.value)
                    notifyDataSetChanged()
                    break
                }
            }
        }
    }

    fun addThrowEndAck(throwEnd: ThrowEndAckObject) {
        for(message in mChatVector) {
            if(message.code != PacketCodes.MESSAGE) {
                val tFile = message as ThrowWrapper
                if(throwEnd.timeID.compareTo(tFile.timeID) == 0) {
                    tFile.setState(ThrowStateCodes.STATE_FINISHED)
                    tFile.setCurrentSize(tFile.fileSize)
                    notifyDataSetChanged()
                    break
                }
            }
        }
    }

    fun nullifySelected() {
        selectedMessages = 0;
        selectedThrows = 0;

        for(item in mChatVector) {
            if(item.code == PacketCodes.MESSAGE) {
                (item as MessageWrapper).setSelected(false)
            } else {
                (item as ThrowWrapper).setSelected(false)
            }
        }

        notifyDataSetChanged()
    }

    fun deleteSelected() {
        val itemsToDelete = Vector<BaseProtocolObject>()

        for(item in mChatVector) {
            if(item.code == PacketCodes.MESSAGE) {
                if ((item as MessageWrapper).getSelected())
                    itemsToDelete.add(item)
            } else {
                if ((item as ThrowWrapper).getSelected())
                    itemsToDelete.add(item)
            }
        }

        for(item in itemsToDelete) {
            if(item.code == PacketCodes.MESSAGE) {
                val message = (item as MessageWrapper)
                DbManager.deleteMessage(message.address, message.timeID)

            } else {
                val tFile = (item as ThrowWrapper)
                DbManager.deleteThrow(tFile.address, tFile.timeID)
            }
            mChatVector.remove(item)
        }

        notifyDataSetChanged()
    }


    private fun setSelected(selected: Boolean, timeID: Date) {
        // find need file and update progress from ThrowProgress
        for(item in mChatVector) {
            if(item.code == PacketCodes.MESSAGE) {
                val message = item as MessageWrapper
                if(message.timeID.compareTo(timeID) == 0) {
                    message.setSelected(selected)
                    when(selected) {
                        true  -> selectedMessages++
                        false -> selectedMessages--
                    }
                    break
                }
            } else {
                val tFile = item as ThrowWrapper
                if(tFile.timeID.compareTo(timeID) == 0) {
                    tFile.setSelected(selected)
                    when(selected) {
                        true  -> selectedThrows++
                        false -> selectedThrows--
                    }
                    break
                }
            }
        }

        if(selectedMessages == 0 && selectedThrows == 0) {
            (ctx as ChatActivity).setSelectedBarVisibility(false)
        } else if(selectedMessages > 0 || selectedThrows > 0) {
            (ctx as ChatActivity).setSelectedBarVisibility(true)
        }

    }



//======================================================================================================================
// RECYCLER VIEW CALLBACKS
//======================================================================================================================

    override fun getItemCount(): Int {
        return mChatVector.size
    }

    // Determines the appropriate ViewType according to the sender of the message.
    override fun getItemViewType(position: Int): Int {

        if(mChatVector[position].code == PacketCodes.MESSAGE) {
            val message = mChatVector[position] as MessageWrapper
            when(message.dir) {
                DirectionCodes.OUTCOMING -> return VIEW_TYPE_MESSAGE_SENT
                DirectionCodes.INCOMING ->  return VIEW_TYPE_MESSAGE_RECEIVED
            }

        } else {
            val tFile = mChatVector[position] as ThrowWrapper
            when(tFile.dir) {
                DirectionCodes.OUTCOMING -> return VIEW_TYPE_FILE_SENT
                DirectionCodes.INCOMING ->  return VIEW_TYPE_FILE_RECEIVED
            }
        }

        return VIEW_TYPE_MESSAGE_SENT
    }

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        val view: View

        when(viewType) {
            VIEW_TYPE_MESSAGE_SENT -> {
                view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_chat_message_outcoming, parent, false)
                return MessageOutcomingHolder(view)
            }

            VIEW_TYPE_MESSAGE_RECEIVED -> {
                view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_chat_message_incoming, parent, false)
                return MessageIncomingHolder(view)
            }

            VIEW_TYPE_FILE_SENT -> {
                view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_chat_file_outcoming, parent, false)
                return SentFileHolder(view)
            }

            VIEW_TYPE_FILE_RECEIVED -> {
                view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_chat_file_incoming, parent, false)
                return ReceivedFileHolder(view)
            }

            else -> return null
        }
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_MESSAGE_SENT ->       (holder as MessageOutcomingHolder).bind(mChatVector[position] as MessageWrapper)
            VIEW_TYPE_MESSAGE_RECEIVED ->   (holder as MessageIncomingHolder).bind(mChatVector[position] as MessageWrapper)
            VIEW_TYPE_FILE_SENT ->          (holder as SentFileHolder).bind(mChatVector[position] as ThrowWrapper)
            VIEW_TYPE_FILE_RECEIVED ->      (holder as ReceivedFileHolder).bind(mChatVector[position] as ThrowWrapper)
        }
    }


//======================================================================================================================
// HOLDERS
//======================================================================================================================

    private inner class MessageOutcomingHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var itemSentContainer = itemView.itemSentContainer
        internal var messageText = itemView.itemSentText
        internal var timeText = itemView.itemSentTime

        internal fun bind(message: MessageWrapper) {
            messageText.text = message.text
            timeText.text = Utils.timeStringFormat(message.timeID)
            showSelected(message.getSelected())

            itemSentContainer.setOnLongClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
                return@setOnLongClickListener true
            }
            itemSentContainer.setOnClickListener {
                if(selectedMessages != 0 || selectedThrows != 0) {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
            }

            messageText.setOnLongClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
                return@setOnLongClickListener true
            }
            messageText.setOnClickListener{
                if(selectedMessages == 0 && selectedThrows == 0) {
                    val clipboard = ctx.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip = ClipData.newPlainText("", messageText.text.toString())
                    clipboard.primaryClip = clip

                    Toast.makeText(ctx, "Copied to clipboard", Toast.LENGTH_SHORT).show()
                } else {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
            }

            setStyle(message.throwMode)
        }

        fun showSelected(selected: Boolean) {
            if(selected) {
                itemSentContainer.setBackgroundColor(ctx.resources.getColor(R.color.SELECTED_CHAT_ITEM))
            } else {
                itemSentContainer.setBackgroundColor(ctx.resources.getColor(R.color.UNSELECTED_CHAT_ITEM))
            }
        }

        fun setStyle(mode: Byte) {
            when(mode) {
                ThrowModeCodes.WIFI_NETWORK -> {
                    messageText.setBackgroundResource(R.drawable.rounded_rectangle_wifi_outcoming)
                    messageText.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_WIFI))
                    messageText.setLinkTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_WIFI))
                }
                ThrowModeCodes.BLUETOOTH -> {
                    messageText.setBackgroundResource(R.drawable.rounded_rectangle_bt_outcoming)
                    messageText.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_BT))
                    messageText.setLinkTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_BT))
                }
            }
        }
    }

    private inner class MessageIncomingHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var itemReceivedContainer = itemView.itemReceivedContainer
        internal var messageText: TextView = itemView.itemReceivedText
        internal var timeText: TextView = itemView.itemReceivedTime

        private var isSelected = false

        internal fun bind(message: MessageWrapper) {
            messageText.text = message.text
            timeText.text = Utils.timeStringFormat(message.timeID)

            showSelected(message.getSelected())

            itemReceivedContainer.setOnLongClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
                return@setOnLongClickListener true
            }
            itemReceivedContainer.setOnClickListener {
                if(selectedMessages != 0 || selectedThrows != 0) {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
            }

            messageText.setOnLongClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
                return@setOnLongClickListener true
            }
            messageText.setOnClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    val clipboard = ctx.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip = ClipData.newPlainText("", messageText.text.toString())
                    clipboard.primaryClip = clip

                    Toast.makeText(ctx, "Copied to clipboard", Toast.LENGTH_SHORT).show()
                } else {
                    message.setSelected(!message.getSelected())
                    showSelected(message.getSelected())
                    setSelected(message.getSelected(), message.timeID)
                }
            }

            setStyle(message.throwMode)
        }

        fun showSelected(selected: Boolean) {
            if(selected) {
                itemReceivedContainer.setBackgroundColor(ctx.resources.getColor(R.color.SELECTED_CHAT_ITEM))
            } else {
                itemReceivedContainer.setBackgroundColor(ctx.resources.getColor(R.color.UNSELECTED_CHAT_ITEM))
            }
        }

        fun setStyle(mode: Byte) {
            when(mode) {
                ThrowModeCodes.WIFI_NETWORK -> {
                    messageText.setBackgroundResource(R.drawable.rounded_rectangle_wifi_incoming)
                    messageText.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_WIFI))
                    messageText.setLinkTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_WIFI))
                }
                ThrowModeCodes.BLUETOOTH -> {
                    messageText.setBackgroundResource(R.drawable.rounded_rectangle_bt_incoming)
                    messageText.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_BT))
                    messageText.setLinkTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_BT))
                }
            }
        }

    }

    private inner class SentFileHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var fileSentRoot = itemView.fileSentRoot
        internal var fileSentContainer = itemView.fileSentContainer
        internal var fileSentImage = itemView.fileSentImage
        internal var fileSentName = itemView.fileSentName
        internal var fileSentProgressBar = itemView.fileSentProgressBar
        internal var fileSentFullSize = itemView.fileSentSize
        internal var fileSentTime = itemView.fileSentTime
        internal var fileSentState = itemView.fileSentStatus

        private var isSelected = false

        internal fun bind(fileThrow: ThrowWrapper) {
            fileSentName.text = fileThrow.filename
            fileSentProgressBar.progress = fileThrow.getProgress()
            fileSentTime.text = Utils.timeStringFormat(fileThrow.timeID)

            setStyle(fileThrow.throwMode)

            showSelected(fileThrow.getSelected())

            fileSentRoot.setOnLongClickListener {
                fileThrow.setSelected(!fileThrow.getSelected())

                showSelected(fileThrow.getSelected())
                setSelected(fileThrow.getSelected(), fileThrow.timeID)

                return@setOnLongClickListener true
            }
            fileSentRoot.setOnClickListener {
                if(selectedMessages != 0 || selectedThrows != 0) {
                    fileThrow.setSelected(!fileThrow.getSelected())
                    showSelected(fileThrow.getSelected())
                    setSelected(fileThrow.getSelected(), fileThrow.timeID)
                }
            }

            fileSentContainer.setOnLongClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    fileThrow.setSelected(!fileThrow.getSelected())
                    showSelected(fileThrow.getSelected())
                    setSelected(fileThrow.getSelected(), fileThrow.timeID)
                }
                return@setOnLongClickListener true
            }
            fileSentContainer.setOnClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    // TODO
                } else {
                    fileThrow.setSelected(!fileThrow.getSelected())
                    showSelected(fileThrow.getSelected())
                    setSelected(fileThrow.getSelected(), fileThrow.timeID)
                }
            }

            val currentSizeStr = Utils.sizeStringFormat(fileThrow.getCurrentSize(), 2)
            val fullSizeStr = Utils.sizeStringFormat(fileThrow.fileSize,2)
            fileSentFullSize.text = "$currentSizeStr / $fullSizeStr"

            when(fileThrow.getState()) {
                ThrowStateCodes.STATE_REQUEST ->     fileSentState.text = ctx.resources.getString(R.string.chat_state_waiting)
                ThrowStateCodes.STATE_REJECTED ->    fileSentState.text = ctx.resources.getString(R.string.chat_state_rejected)
                ThrowStateCodes.STATE_PROCESS ->     fileSentState.text = ctx.resources.getString(R.string.chat_state_uploading)
                ThrowStateCodes.STATE_INTERRUPTED -> fileSentState.text = ctx.resources.getString(R.string.chat_state_interrupted)
                ThrowStateCodes.STATE_FINISHED ->    fileSentState.text = ctx.resources.getString(R.string.chat_state_finished)
                ThrowStateCodes.STATE_DELETED ->     fileSentState.text = ctx.resources.getString(R.string.chat_state_deleted)
            }
        }

        fun showSelected(selected: Boolean) {
            if(selected) {
                fileSentRoot.setBackgroundColor(ctx.resources.getColor(R.color.SELECTED_CHAT_ITEM))
            } else {
                fileSentRoot.setBackgroundColor(ctx.resources.getColor(R.color.UNSELECTED_CHAT_ITEM))
            }
        }

        fun setStyle(mode: Byte) {
            when(mode) {
                ThrowModeCodes.WIFI_NETWORK -> {
                    fileSentImage.setImageResource(R.drawable.file_black_512)
                    fileSentContainer.setBackgroundResource(R.drawable.rounded_rectangle_wifi_outcoming)
                    fileSentImage
                    fileSentName.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_WIFI))
                    fileSentFullSize.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_WIFI))
                    fileSentState.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_WIFI))
                    fileSentProgressBar.setProgressTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_WIFI))
                    fileSentProgressBar.reachedBarColor = ctx.resources.getColor(R.color.TEXT_OUTCOMING_WIFI)
                    fileSentProgressBar.unreachedBarColor = ctx.resources.getColor(R.color.PROGRESS_BAR_UNREACHED)
                }
                ThrowModeCodes.BLUETOOTH -> {
                    fileSentImage.setImageResource(R.drawable.file_white_512)
                    fileSentContainer.setBackgroundResource(R.drawable.rounded_rectangle_bt_outcoming)
                    fileSentImage
                    fileSentName.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_BT))
                    fileSentFullSize.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_BT))
                    fileSentState.setTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_BT))
                    fileSentProgressBar.setProgressTextColor(ctx.resources.getColor(R.color.TEXT_OUTCOMING_BT))
                    fileSentProgressBar.reachedBarColor = ctx.resources.getColor(R.color.TEXT_OUTCOMING_BT)
                    fileSentProgressBar.unreachedBarColor = ctx.resources.getColor(R.color.PROGRESS_BAR_UNREACHED)
                }
            }
        }
    }

    private inner class ReceivedFileHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var fileReceivedRoot = itemView.fileReceivedRoot
        internal var fileReceivedContainer = itemView.fileReceivedContainer
        internal var fileReceivedImage = itemView.fileReceivedImage
        internal var fileReceivedName = itemView.fileReceivedName
        internal var fileReceivedProgressBar = itemView.fileReceivedProgressBar
        internal var fileReceivedFullSize = itemView.fileReceivedSize
        internal var fileReceivedTime = itemView.fileReceivedTime
        internal var fileReceivedState = itemView.fileReceivedStatus


        internal fun bind(fileThrow: ThrowWrapper) {
            fileReceivedName.text = fileThrow.filename
            fileReceivedProgressBar.progress = fileThrow.getProgress()
            fileReceivedTime.text = Utils.timeStringFormat(fileThrow.timeID)

            setStyle(fileThrow.throwMode)

            showSelected(fileThrow.getSelected())

            fileReceivedRoot.setOnLongClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    fileThrow.setSelected(!fileThrow.getSelected())
                    showSelected(fileThrow.getSelected())
                    setSelected(fileThrow.getSelected(), fileThrow.timeID)
                }
                return@setOnLongClickListener true
            }
            fileReceivedRoot.setOnClickListener {
                if(selectedMessages != 0 || selectedThrows != 0) {
                    fileThrow.setSelected(!fileThrow.getSelected())
                    showSelected(fileThrow.getSelected())
                    setSelected(fileThrow.getSelected(), fileThrow.timeID)
                }
            }

            fileReceivedContainer.setOnLongClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    fileThrow.setSelected(!fileThrow.getSelected())
                    showSelected(fileThrow.getSelected())
                    setSelected(fileThrow.getSelected(), fileThrow.timeID)
                }
                return@setOnLongClickListener true
            }
            fileReceivedContainer.setOnClickListener {
                if(selectedMessages == 0 && selectedThrows == 0) {
                    if(fileThrow.path != null) {
                        val newIntent = Intent(Intent.ACTION_VIEW)

                        val file = File(fileThrow.path)
                        val mime = MimeTypeMap.getSingleton()
                        val index = file.getName().lastIndexOf('.') + 1
                        val ext = file.getName().substring(index).toLowerCase()
                        val type = mime.getMimeTypeFromExtension(ext)
                        val data = Uri.fromFile(File(fileThrow.path))

                        log.info("data = " + data)
                        log.info("mime = " + type)

                        newIntent.setDataAndType(data, type)
                        newIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        try {
                            ctx.startActivity(newIntent)
                        } catch (e: ActivityNotFoundException) {
                            Toast.makeText(ctx, "No handler for this type of file", Toast.LENGTH_LONG).show()
                        }
                    }

                } else {
                    fileThrow.setSelected(!fileThrow.getSelected())
                    showSelected(fileThrow.getSelected())
                    setSelected(fileThrow.getSelected(), fileThrow.timeID)
                }
            }

            val currentSizeStr = Utils.sizeStringFormat(fileThrow.getCurrentSize(), 2)
            val fullSizeStr = Utils.sizeStringFormat(fileThrow.fileSize,2)
            fileReceivedFullSize.text = "$currentSizeStr / $fullSizeStr"


            when(fileThrow.getState()) {
                ThrowStateCodes.STATE_REQUEST ->     fileReceivedState.text = ctx.resources.getString(R.string.chat_state_waiting)
                ThrowStateCodes.STATE_REJECTED ->    fileReceivedState.text = ctx.resources.getString(R.string.chat_state_rejected)
                ThrowStateCodes.STATE_PROCESS ->     fileReceivedState.text = ctx.resources.getString(R.string.chat_state_uploading)
                ThrowStateCodes.STATE_INTERRUPTED -> fileReceivedState.text = ctx.resources.getString(R.string.chat_state_interrupted)
                ThrowStateCodes.STATE_FINISHED ->    {
                    fileReceivedState.text = ctx.resources.getString(R.string.chat_state_finished)
                    val tFile = DbManager.selectThrow(fileThrow.address, fileThrow.timeID)
                    fileThrow.path = tFile.path
                }
                ThrowStateCodes.STATE_DELETED ->     fileReceivedState.text = ctx.resources.getString(R.string.chat_state_deleted)
            }
        }

        fun showSelected(selected: Boolean) {
            if(selected) {
                fileReceivedRoot.setBackgroundColor(ctx.resources.getColor(R.color.SELECTED_CHAT_ITEM))
            } else {
                fileReceivedRoot.setBackgroundColor(ctx.resources.getColor(R.color.UNSELECTED_CHAT_ITEM))
            }
        }

        fun setStyle(mode: Byte) {
            when(mode) {
                ThrowModeCodes.WIFI_NETWORK -> {
                    fileReceivedImage.setImageResource(R.drawable.file_black_512)
                    fileReceivedContainer.setBackgroundResource(R.drawable.rounded_rectangle_wifi_incoming)
                    fileReceivedImage
                    fileReceivedName.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_WIFI))
                    fileReceivedFullSize.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_WIFI))
                    fileReceivedState.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_WIFI))
                    fileReceivedProgressBar.setProgressTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_WIFI))
                    fileReceivedProgressBar.reachedBarColor = ctx.resources.getColor(R.color.TEXT_INCOMING_WIFI)
                    fileReceivedProgressBar.unreachedBarColor = ctx.resources.getColor(R.color.PROGRESS_BAR_UNREACHED)
                }
                ThrowModeCodes.BLUETOOTH -> {
                    fileReceivedImage.setImageResource(R.drawable.file_white_512)
                    fileReceivedContainer.setBackgroundResource(R.drawable.rounded_rectangle_bt_incoming)
                    fileReceivedImage
                    fileReceivedName.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_BT))
                    fileReceivedFullSize.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_BT))
                    fileReceivedState.setTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_BT))
                    fileReceivedProgressBar.setProgressTextColor(ctx.resources.getColor(R.color.TEXT_INCOMING_BT))
                    fileReceivedProgressBar.reachedBarColor = ctx.resources.getColor(R.color.TEXT_INCOMING_BT)
                    fileReceivedProgressBar.unreachedBarColor = ctx.resources.getColor(R.color.PROGRESS_BAR_UNREACHED)
                }
            }
        }
    }


    private fun fileExt(url: String): String? {
        var url = url
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"))
        }
        if (url.lastIndexOf(".") == -1) {
            return null
        } else {
            var ext = url.substring(url.lastIndexOf(".") + 1)
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"))
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"))
            }
            return ext.toLowerCase()

        }
    }

    private fun getMimeType(url: String): String? {
        var type: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return type
    }
}
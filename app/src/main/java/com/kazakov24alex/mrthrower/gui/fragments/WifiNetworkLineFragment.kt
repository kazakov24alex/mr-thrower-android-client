package com.kazakov24alex.mrthrower.gui.fragments


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.kazakov24alex.mrthrower.R
import com.kazakov24alex.mrthrower.gui.activities.ContactsActivity
import com.kazakov24alex.mrthrower.utils.experts.BluetoothExpert
import com.kazakov24alex.mrthrower.utils.experts.WifiNetworkExpert
import kotlinx.android.synthetic.main.fragment_wifi_network_line.view.*
import library.codes.ThrowModeCodes


class WifiNetworkLineFragment : Fragment() {
    private val WIFI_IMAGE_RESOURCE = R.drawable.wifi_black_512

    @get:JvmName("getContext_") private lateinit var view: View

    private var mode: Byte = -1



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        view = inflater!!.inflate(R.layout.fragment_wifi_network_line, container, false)

        view.viewWifiLineImage.setImageResource(WIFI_IMAGE_RESOURCE)

        view.viewWifiLineImageContainer.setOnClickListener() {
            val contactsActivity: ContactsActivity = activity as ContactsActivity
            contactsActivity.onChangeMode()

        }

        view.viewWifiLineSettingContainer.setOnClickListener() {
            Toast.makeText(context, "SETTINGS", Toast.LENGTH_SHORT).show()
        }

        when(mode) {
            ThrowModeCodes.WIFI_NETWORK -> view.viewWifiLineSSID.text = WifiNetworkExpert.getWiFiSSID(context)
            ThrowModeCodes.BLUETOOTH -> view.viewWifiLineSSID.text = BluetoothExpert.getName()
            ThrowModeCodes.WIFI_DIRECT -> view.viewWifiLineSSID.text = "HISTORY"
        }

        when(mode) {
            ThrowModeCodes.WIFI_NETWORK -> view.viewWifiLineImage.setImageDrawable(resources.getDrawable(R.drawable.wifi_black_512))
            ThrowModeCodes.BLUETOOTH -> view.viewWifiLineImage.setImageDrawable(resources.getDrawable(R.drawable.bluetooth_black_512))
            ThrowModeCodes.WIFI_DIRECT -> view.viewWifiLineImage.setImageDrawable(resources.getDrawable(R.drawable.direct_black_512))
        }

        when(mode) {
            ThrowModeCodes.WIFI_NETWORK -> {
                view.viewWifiLineSSIDContainer.setOnClickListener() {
                    startActivity(Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            }
            ThrowModeCodes.BLUETOOTH -> {
                view.viewWifiLineSSIDContainer.setOnClickListener() {
                    startActivity(Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
                }
            }
        }

        // Inflate the layout for this fragment
        return view
    }


    fun setMode(mode: Byte) {
        this.mode = mode
    }


}

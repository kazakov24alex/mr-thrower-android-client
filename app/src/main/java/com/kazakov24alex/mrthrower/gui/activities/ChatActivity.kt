package com.kazakov24alex.mrthrower.gui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kazakov24alex.mrthrower.R

import android.provider.MediaStore
import android.support.v7.widget.LinearLayoutManager
import com.kazakov24alex.mrthrower.gui.adapters.MessageListAdapter
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.kazakov24alex.mrthrower.utils.experts.WifiNetworkExpert
import com.kazakov24alex.mrthrower.service.AppService
import kotlinx.android.synthetic.main.activity_chat.*
import library.codes.PacketCodes
import library.codes.ThrowModeCodes
import library.interfaces.event_manager.IEventListener
import library.objects.*
import library.wrappers.MD5
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import java.util.logging.Logger
import java.io.File
import library.objects.ThrowFileObject
import library.wrappers.EventWrapper
import library.wrappers.IpAddress
import library.wrappers.MacAddress


class ChatActivity : AppCompatActivity(), IEventListener {

    companion object {
        val log = Logger.getLogger(AppService::class.java.name)
        private val PICKFILE_RESULT_CODE = 123
    }


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private lateinit var mContact: GreetingObject
    private var isOnline = true
    private var mode: Byte = -1

    private lateinit var mModeImage: ImageView
    private lateinit var mNameText: TextView

    private lateinit var mChatRecycler: RecyclerView
    private lateinit var mChatAdapter: MessageListAdapter

    private lateinit var mInputLineContainer: LinearLayout
    private lateinit var mMessageTextView: TextView
    private lateinit var mSendButton: ImageView
    private lateinit var mOfflineText: TextView

    private lateinit var mChatCloseImage: ImageView
    private lateinit var mChatDeleteSelected: ImageView


    private val messageList: Vector<BaseProtocolObject> = Vector()


//======================================================================================================================
// ACTIVITY OVERRIDE
//======================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        mModeImage = findViewById(R.id.chatModeImage)
        mode = intent.getByteExtra(WelcomeActivity.INTENT_EXTRA_MODE,-1)
        when(mode) {
            ThrowModeCodes.WIFI_NETWORK -> chatModeImage.setImageResource(R.drawable.wifi_black_512)
            ThrowModeCodes.BLUETOOTH -> chatModeImage.setImageResource(R.drawable.bluetooth_black_512)
            ThrowModeCodes.WIFI_DIRECT -> chatModeImage.setImageResource(R.drawable.direct_black_512)
        }

        // EventBus register
        EventBus.getDefault().register(this)

        // Contact data extracting
        mContact = GreetingObject(
                MacAddress(intent.getByteArrayExtra("MAC")),
                IpAddress(intent.getByteArrayExtra("IP")),
                intent.getStringExtra("NAME"),
                intent.getStringExtra("DEVICE"),
                intent.getByteExtra("OS", GreetingObject.OS_CODE_WINDOWS))


        mNameText = findViewById(R.id.chatNameText)
        mNameText.text = mContact.name // + " [" + mContact.macAddress + "]"

        // ChatRecycler view initialization
        mChatRecycler = findViewById(R.id.reyclerview_message_list)
        mChatAdapter = MessageListAdapter(this, mode, mContact.macAddress, messageList)
        mChatAdapter.loadFromDatabase()
        mChatRecycler.adapter = mChatAdapter
        mChatRecycler.layoutManager = LinearLayoutManager(this)

        mInputLineContainer = findViewById(R.id.chatInputLineContainer)
        mOfflineText = findViewById(R.id.chatOfflineText)

        mOfflineText.visibility = View.INVISIBLE
        mInputLineContainer.visibility = View.VISIBLE

        // Text message and Send button initialization
        mMessageTextView = findViewById(R.id.chatTextField)
        mMessageTextView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(p0.toString().isEmpty()) {
                    mSendButton.setImageResource(R.drawable.attach_512)
                } else {
                    mSendButton.setImageResource(R.drawable.send_512)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })


        mSendButton = findViewById(R.id.chatButtonSend)

        // Set callback to Message send and File throw
        mSendButton.setOnClickListener {
            // File throw callback
            if (mMessageTextView.text.isEmpty()) {
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "file/*"
                startActivityForResult(intent, PICKFILE_RESULT_CODE)

                // Send message callback
            } else {
                val message = MessageObject(Date(), mMessageTextView.text.toString())
                message.address = mContact.macAddress
                EventBus.getDefault().post(EventWrapper(message, EventWrapper.TO_EVENT_MANAGER))

                mMessageTextView.text = ""
                message.address = WifiNetworkExpert.getMacAddress(this)
                mChatAdapter.addMessage(message)
                mChatRecycler.scrollToPosition(mChatAdapter.itemCount - 1)
            }
        }


        mChatCloseImage = findViewById(R.id.chatCloseImage)
        mChatDeleteSelected = findViewById(R.id.chatDeleteSelected)
        setSelectedBarVisibility(false)

        mChatCloseImage.setOnClickListener {
            setSelectedBarVisibility(false)
            mChatAdapter.nullifySelected()
        }
        mChatDeleteSelected.setOnClickListener{
            setSelectedBarVisibility(false)
            mChatAdapter.deleteSelected()
            mChatAdapter.nullifySelected()
        }


        if(mode == ThrowModeCodes.WIFI_DIRECT)
            offlineMode(true)
    }

    override fun onResume() {
        super.onResume()
        mChatRecycler.scrollToPosition(mChatAdapter.itemCount - 1)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)

        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode != PICKFILE_RESULT_CODE)
            return

        if(data == null)
            return

        // File was chosen
        if (resultCode == Activity.RESULT_OK) {
            val uri = data.data
            var filepath = uri.path

            // convert uri to absolute path format if it is necessary
            if(data.data.toString().contains("content://")) {
                val projection = arrayOf(MediaStore.Images.Media.DATA)
                val cursor = getContentResolver().query(uri, projection, null, null, null)
                cursor.moveToFirst()
                val columnIndex = cursor.getColumnIndex(projection[0])
                filepath = cursor.getString(columnIndex)
                cursor.close()
            }

            val request = ThrowRequestObject()

            val file = File(filepath)

            // TODO md5 checksum

            val md5bytes = byteArrayOf(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)

            val tFile = ThrowFileObject(Date(), file.name, file.length(), MD5(md5bytes))
            tFile.path = filepath

            request.addThrowFile(tFile)


            // Send ThrowRequest via EventManager
            request.address = WifiNetworkExpert.getMacAddress(this)
            mChatAdapter.addThrowRequest(request)
            mChatRecycler.scrollToPosition(mChatAdapter.itemCount - 1)

            request.address = mContact.macAddress
            EventBus.getDefault().post(EventWrapper(request, EventWrapper.TO_EVENT_MANAGER))


        }
    }

    fun setSelectedBarVisibility(visible: Boolean) {
        if(visible) {
            mChatCloseImage.visibility = View.VISIBLE
            mChatDeleteSelected.visibility = View.VISIBLE
        } else {
            mChatCloseImage.visibility = View.INVISIBLE
            mChatDeleteSelected.visibility = View.INVISIBLE
        }
    }

    private fun offlineMode(on: Boolean) {
        if(on) {
            isOnline = false
            mOfflineText.visibility = View.VISIBLE
            mInputLineContainer.visibility = View.INVISIBLE
        } else {
            isOnline = true
            mOfflineText.visibility = View.INVISIBLE
            mInputLineContainer.visibility = View.VISIBLE
        }
    }
//=============
//
// =========================================================================================================
// EVENT LISTENER AND DISTRIBUTOR
//======================================================================================================================

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onEvent(event: EventWrapper) {
        // check event direction
        if(event.getDirection() != EventWrapper.FROM_EVENT_MANAGER)
            return

        // check sender
        if(event.getObject().address.compareTo(mContact.macAddress) != 0)
            return

        // identify the type of event and select the callback
        when(event.getCode()) {
            PacketCodes.GREETING ->         onGreetingEvent(event.getObject() as GreetingObject)
            PacketCodes.GOODBYE ->          onGoodbyeEvent(event.getObject() as GoodbyeObject)
            PacketCodes.MESSAGE ->          onMessageEvent(event.getObject() as MessageObject)
            PacketCodes.MESSAGE_ACK ->      onMessageAckEvent(event.getObject() as MessageAckObject)
            PacketCodes.THROW_REQUEST ->    onThrowRequestEvent(event.getObject() as ThrowRequestObject)
            PacketCodes.THROW_RESPONSE ->   onThrowResponseEvent(event.getObject() as ThrowResponseObject)
            PacketCodes.THROW_PROGRESS ->   onThrowProgressEvent(event.getObject() as ThrowProgressObject)
            PacketCodes.THROW_END_ACK ->    onThrowEndAckEvent(event.getObject() as ThrowEndAckObject)
        }
    }


//======================================================================================================================
// EVENT CALLBACKS
//======================================================================================================================

    override fun onGreetingEvent(greeting: GreetingObject) {
        if(greeting.macAddress.compareTo(mContact.macAddress) == 0) {
            offlineMode(false)
        }
    }

    override fun onGoodbyeEvent(goodbye: GoodbyeObject) {
        if(goodbye.mac.compareTo(mContact.macAddress) == 0) {
            offlineMode(true)
        }
    }

    override fun onMessageEvent(message: MessageObject) {
        // add message to the chat recycler view
        mChatAdapter.addMessage(message)
        mChatRecycler.scrollToPosition(mChatAdapter.itemCount-1)
    }

    override fun onMessageAckEvent(messageAck: MessageAckObject) {
        // TODO mark message as taken
    }

    override fun onThrowRequestEvent(request: ThrowRequestObject) {
        // TODO approval dialog

        // add request on chat recycler view
        mChatAdapter.addThrowRequest(request)
        mChatRecycler.scrollToPosition(mChatAdapter.itemCount - 1)


        // send taken request to ThrowManager
        EventBus.getDefault().post(EventWrapper(request, EventWrapper.TO_THROW_MANAGER))
    }

    override fun onThrowResponseEvent(response: ThrowResponseObject) {
        // display response on chat recycler view
        mChatAdapter.addThrowResponse(response)
    }

    override fun onThrowProgressEvent(progress: ThrowProgressObject) {
        // display progress on chat recycler view
        mChatAdapter.addThrowProgress(progress)
    }

    override fun onThrowEndAckEvent(throwEnd: ThrowEndAckObject) {
        // display throw finish on chat recycler view
        mChatAdapter.addThrowEndAck(throwEnd)
    }
}

package com.kazakov24alex.mrthrower.gui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kazakov24alex.mrthrower.R
import kotlinx.android.synthetic.main.fragment_contact.*


class ContactFragment : Fragment() {

    companion object {
        const val OS_WINDOWS = 0
        const val OS_ANDROID = 1
    }

    private val osImages = intArrayOf(
            R.drawable.windows_black_512,
            R.drawable.android_black_512
    )

    var deviceName = "NONAME"
        set(value) { field = value; viewContactName.text = value }
    var ipAddress = "0.0.0.0"
        set(value) { field = value; viewContactDeviceName.text = value }
    var os: Int = 0
        set(value) { field = value; viewContactOsImage.setImageResource(osImages[os]) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_profile, container, false)
    }

}

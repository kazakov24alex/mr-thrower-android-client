package com.kazakov24alex.mrthrower.gui.activities

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import com.kazakov24alex.mrthrower.R
import com.kazakov24alex.mrthrower.database.DbManager
import com.kazakov24alex.mrthrower.gui.adapters.ContactsAdapter
import com.kazakov24alex.mrthrower.utils.wrappers.ContactWrapper
import com.kazakov24alex.mrthrower.gui.fragments.ProfileFragment
import com.kazakov24alex.mrthrower.gui.fragments.WifiNetworkLineFragment
import com.kazakov24alex.mrthrower.utils.experts.WifiNetworkExpert
import com.kazakov24alex.mrthrower.service.AppService
import kotlinx.android.synthetic.main.activity_contacts.*
import library.codes.OsCodes
import library.codes.PacketCodes
import library.codes.ThrowModeCodes
import library.interfaces.event_manager.IEventListener
import library.objects.*
import library.wrappers.EventWrapper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe
import java.util.logging.Logger


class ContactsActivity : AppCompatActivity(), IEventListener {

    companion object {
        val log = Logger.getLogger(AppService::class.java.name)

        val BT_DISCOVERABLE_CODE = 222
    }


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var mode: Byte = -1

    private lateinit var profile: ProfileFragment
    private lateinit var contactsAdapter: ContactsAdapter
    private val contactsList: ArrayList<ContactWrapper> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
        mode = intent.getByteExtra(WelcomeActivity.INTENT_EXTRA_MODE,-1)

        profile = viewProfileFragment as ProfileFragment
        profile.contactName = DbManager.getProfile().name
        profile.deviceName = DbManager.getProfile().device
        profile.os = OsCodes.ANDROID


        val wnlf = WifiNetworkLineFragment()
        supportFragmentManager
                .beginTransaction()
                .add(R.id.viewProfileLineContainer, wnlf, "rageComicList")
                .commit()
        WifiNetworkExpert.getWiFiSSID(this)
        wnlf.setMode(mode)


        contactsAdapter = ContactsAdapter(this, contactsList)
        viewProfileContactsListView?.adapter = contactsAdapter
        contactsAdapter.notifyDataSetChanged()

        viewProfileContactsListView.setOnItemClickListener { adapterView, view, pos, id ->
            val intent = Intent(this, ChatActivity::class.java)
            intent.putExtra(WelcomeActivity.INTENT_EXTRA_MODE, mode)

            intent.putExtra("MAC", contactsList[pos].macAddress.bytes)
            intent.putExtra("IP", contactsList[pos].ipAddress.bytes)
            intent.putExtra("NAME", contactsList[pos].name)
            intent.putExtra("DEVICE", contactsList[pos].device)
            intent.putExtra("OS", contactsList[pos].os)

            startActivity(intent)
        }

        // start service
        when(mode) {
            ThrowModeCodes.WIFI_NETWORK -> {
                startService()
            }
            ThrowModeCodes.BLUETOOTH -> {
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
                intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0)
                ActivityCompat.startActivityForResult(this, intent, BT_DISCOVERABLE_CODE, null)
            }
            ThrowModeCodes.WIFI_DIRECT -> {
                startService()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode != BT_DISCOVERABLE_CODE)
            return

        if (resultCode == Activity.RESULT_FIRST_USER) {
            startService()
        } else {
            Toast.makeText(this, "?", Toast.LENGTH_SHORT).show()
        }
    }



    fun startService() {
        val intent = Intent(this, AppService::class.java)
        intent.putExtra(WelcomeActivity.INTENT_EXTRA_MODE, mode)
        startService(intent)
    }

    fun onChangeMode() {
        onBackPressed()
    }


    override fun onBackPressed() {
        stopService(Intent(this, AppService::class.java))
        super.onBackPressed()

    }

    override fun onStart() {
        // start WiFi service
        EventBus.getDefault().register(this)

        super.onStart()
    }

    override fun onStop() {
        // start WiFi service
        EventBus.getDefault().unregister(this)

        log.info("ON STOP!")
        super.onStop()
    }

    override fun onDestroy() {
        log.info("ON DESTROY!")
        stopService(Intent(this, AppService::class.java))
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        contactsAdapter.clearContactsList()
        EventBus.getDefault().post(EventWrapper(ContactsRequest(), EventWrapper.TO_EVENT_MANAGER))
    }

//======================================================================================================================
// EVENT CALLBACKS
//======================================================================================================================

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onEvent(event: EventWrapper) {
        if(event.getDirection() != EventWrapper.FROM_EVENT_MANAGER) {
            return
        }

        when(event.getCode()) {
            PacketCodes.GREETING ->         onGreetingEvent(event.getObject() as GreetingObject)
            PacketCodes.GOODBYE ->          onGoodbyeEvent(event.getObject() as GoodbyeObject)
            PacketCodes.MESSAGE ->          onMessageEvent(event.getObject() as MessageObject)
            PacketCodes.MESSAGE_ACK ->      onMessageAckEvent(event.getObject() as MessageAckObject)
            PacketCodes.THROW_REQUEST ->    onThrowRequestEvent(event.getObject() as ThrowRequestObject)
            PacketCodes.THROW_RESPONSE ->   onThrowResponseEvent(event.getObject() as ThrowResponseObject)
            PacketCodes.THROW_PROGRESS ->   onThrowProgressEvent(event.getObject() as ThrowProgressObject)
            PacketCodes.THROW_END_ACK ->    onThrowEndAckEvent(event.getObject() as ThrowEndAckObject)

        }
    }

    override fun onGreetingEvent(greeting: GreetingObject) {
        contactsAdapter.addContact(greeting)
    }

    override fun onGoodbyeEvent(goodbye: GoodbyeObject) {
        contactsAdapter.removeContact(goodbye.address)
    }

    override fun onMessageEvent(message: MessageObject) {
        contactsAdapter.incrementMesCounter(message.address)
    }

    override fun onMessageAckEvent(messageAck: MessageAckObject) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onThrowRequestEvent(request: ThrowRequestObject) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onThrowResponseEvent(response: ThrowResponseObject) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onThrowProgressEvent(progress: ThrowProgressObject) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onThrowEndAckEvent(throwEnd: ThrowEndAckObject) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}

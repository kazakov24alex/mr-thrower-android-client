package com.kazakov24alex.mrthrower.utils.wrappers

import library.codes.ThrowStateCodes
import library.objects.ThrowFileObject
import library.wrappers.MD5
import java.util.*

class ThrowWrapper(val throwFile: ThrowFileObject, val dir: Byte, val throwMode: Byte, val throwType: Byte)
            : ThrowFileObject(throwFile.timeID, throwFile.filename, throwFile.fileSize, throwFile.mD5) {

    constructor(timeID: Date, filename: String, size: Long, md5: MD5, dir: Byte, throwMode: Byte, throwType: Byte)
            : this(ThrowFileObject(timeID, filename, size, md5), dir, throwMode, throwType)


    private var state = ThrowStateCodes.STATE_REQUEST
    private var currentSize: Long = 0
    private var isSelected = false


    init {
        path = throwFile.path
        address = throwFile.address
    }

    fun getProgress(): Int {
        return ((currentSize.toFloat() / fileSize.toFloat()) * 100).toInt()
    }

    fun getCurrentSize(): Long {
        return currentSize
    }

    fun setCurrentSize(currentSize: Long) {
        this.currentSize = currentSize
    }


    /*fun getDirection() = dir
    fun getMode() = throwMode
    */

    fun getState() = state
    fun setState(state: Byte) {
        this.state = state

        when(state) {
            ThrowStateCodes.STATE_FINISHED  -> currentSize = fileSize
            ThrowStateCodes.STATE_DELETED   -> currentSize = fileSize
        }
    }


    fun getSelected() = isSelected
    fun setSelected(selected: Boolean) {
        isSelected = selected
    }


}
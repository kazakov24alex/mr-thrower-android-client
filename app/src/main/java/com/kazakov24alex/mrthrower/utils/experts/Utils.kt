package com.kazakov24alex.mrthrower.utils.experts

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    val kB: Long = 1024
    val MB: Long = kB * 1024
    val GB: Long = MB * 1024


    fun sizeStringFormat(filesize: Long, round: Int): String {
        var strPattern = "#."
        for(i in 0 until round) {
            strPattern += "#"
        }

        val df = DecimalFormat(strPattern)
        df.roundingMode = RoundingMode.CEILING

        if(filesize < kB) {
            return "$filesize b"
        } else if(filesize < MB) {
            val kb = filesize.toFloat() / kB.toFloat()
            return df.format(kb) + " kB"
        } else if(filesize < GB) {
            val mb = filesize.toFloat() / MB.toFloat()
            return df.format(mb) + " MB"
        } else {
            val gb = filesize.toFloat() / GB.toFloat()
            return df.format(gb) + " GB"
        }
    }

    fun timeStringFormat(date: Date): String {
        val df = SimpleDateFormat("hh:mm")
        return df.format(date)
    }

}
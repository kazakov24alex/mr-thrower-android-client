package com.kazakov24alex.mrthrower.utils.wrappers

import library.codes.ThrowStateCodes
import library.objects.MessageObject
import java.util.*

class MessageWrapper(val message: MessageObject, val dir: Byte, val throwMode: Byte)
            : MessageObject(message.timeID, message.text) {

    constructor(timeID: Date, text: String, dir: Byte, throwMode: Byte)
            : this(MessageObject(timeID, text), dir, throwMode)


    private var state = ThrowStateCodes.STATE_REQUEST
    private var isSelected = false

    init {
        address = message.address
    }

    fun getState() = state
    fun setState(state: Byte) {
        this.state = state
    }

    fun getSelected() = isSelected
    fun setSelected(selected: Boolean) {
        isSelected = selected
    }

}
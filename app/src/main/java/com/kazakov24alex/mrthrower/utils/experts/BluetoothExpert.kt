package com.kazakov24alex.mrthrower.utils.experts

import android.bluetooth.BluetoothAdapter
import library.wrappers.MacAddress


object BluetoothExpert {

    fun isAvailable(): Boolean {
        return BluetoothAdapter.getDefaultAdapter() != null
    }

    fun isEnabled(): Boolean {
        return BluetoothAdapter.getDefaultAdapter().isEnabled
    }

    fun getName(): String {
        return BluetoothAdapter.getDefaultAdapter().name
    }

    fun getMacAddress(): MacAddress {
        return MacAddress(BluetoothAdapter.getDefaultAdapter().address)
    }

    fun getDeviceName(): String {
        return android.os.Build.MODEL;
    }


}
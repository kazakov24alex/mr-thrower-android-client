package com.kazakov24alex.mrthrower.utils.experts

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.wifi.WifiManager
import android.text.format.Formatter
import library.wrappers.IpAddress
import library.wrappers.MacAddress


object WifiNetworkExpert {

    fun getIpAddress(ctx: Context): IpAddress {
        val manager = ctx.getSystemService(WIFI_SERVICE) as WifiManager
        val ipString = Formatter.formatIpAddress(manager.connectionInfo.ipAddress)

        return IpAddress(ipString)
    }

    fun getMacAddress(ctx: Context): MacAddress {
        val manager = ctx.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val address = manager.connectionInfo.macAddress

        return MacAddress(address)
    }

    fun getWiFiSSID(ctx: Context): String {
        val wifiManager = ctx.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiInfo = wifiManager.connectionInfo


        return if(wifiInfo.ssid.contains("unknown"))
            "no network"
        else
            wifiInfo.ssid
    }


    fun getDeviceName(): String {
        return android.os.Build.MODEL;
    }
}
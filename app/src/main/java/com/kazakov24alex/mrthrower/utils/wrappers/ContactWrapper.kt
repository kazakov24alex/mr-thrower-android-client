package com.kazakov24alex.mrthrower.utils.wrappers

import library.objects.GreetingObject


class ContactWrapper(greeting: GreetingObject) : GreetingObject(
        greeting.macAddress,
        greeting.ipAddress,
        greeting.name,
        greeting.device,
        greeting.os) {

    private var mesCounter: Int = 0
    private var dbID: Int = 0

    fun setDbID(id: Int) {
        dbID = id
    }

    fun getDbId() = dbID

    fun getCounterValue(): Int {
        return mesCounter
    }

    fun incrementUnreadCounter() {
        mesCounter++
    }

    fun setUnreadCounter(num: Int) {
        mesCounter = num
    }

    fun nullifyCounter() {
        mesCounter = 0
    }


}
package com.kazakov24alex.mrthrower

import android.app.Application


class MrThrowerApp : Application() {
    companion object {
        lateinit var instance: MrThrowerApp
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}
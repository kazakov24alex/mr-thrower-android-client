package com.kazakov24alex.mrthrower.service

import com.kazakov24alex.mrthrower.service.bluetooth_module.BtCatchThread
import com.kazakov24alex.mrthrower.service.bluetooth_module.BtThrowThread
import com.kazakov24alex.mrthrower.service.wifi_module.WifiCatchThread
import com.kazakov24alex.mrthrower.service.wifi_module.WifiThrowThread
import library.codes.ThrowModeCodes
import library.interfaces.event_manager.IEventThrow
import library.interfaces.throw_manager.IThrowManager
import library.interfaces.throw_manager.IThrowManagerInformer
import library.interfaces.throw_threads.ICatchThread
import library.interfaces.throw_threads.IThrowThread
import library.objects.ThrowFileObject
import java.util.*
import java.util.logging.Logger
import library.wrappers.MacAddress
import library.wrappers.IpAddress
import library.objects.ThrowEndAckObject


class ThrowManager(
        private val eventManager: IEventThrow,
        private val mode: Byte) : IThrowManager, IThrowManagerInformer {

    companion object { val log = Logger.getLogger(ThrowManager::class.java.name) }


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var queueFilesToThrow: Vector<ThrowFileObject> = Vector()
    private var queueFilesToCatch: Vector<ThrowFileObject> = Vector()

    private var activeThrowThreads: MutableMap<String, IThrowThread> = mutableMapOf()
    private var activeCatchThreads: MutableMap<String, ICatchThread> = mutableMapOf()


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        log.info("throw manager initialized")
    }

    override fun stop() {
        // stop active throws and delete from active
        for (pair in activeCatchThreads.entries) {
            pair.value.deactivate()
        }
        for (pair in activeThrowThreads.entries) {
            pair.value.deactivate()
        }

        // clear queues
        synchronized(queueFilesToThrow) {
            activeCatchThreads.clear()
            activeThrowThreads.clear()

            queueFilesToCatch.clear()
            queueFilesToThrow.clear()
        }
    }


//======================================================================================================================
// ADDING THROWS
//======================================================================================================================

    override fun addThrowFiles(tFileList: List<ThrowFileObject>) {
        synchronized(queueFilesToThrow) {
            queueFilesToThrow.addAll(tFileList)
        }
        log.info("" + tFileList.size + " throws was added to throw list")
    }

    override fun addCatchFiles(tFileList: List<ThrowFileObject>) {
        synchronized(queueFilesToCatch) {
            queueFilesToCatch.addAll(tFileList)
        }
        log.info("" + tFileList.size + " catches was added to catch list")

        startCatchIfNotYet(tFileList.get(0).address)
    }


//======================================================================================================================
// START IF NOT YET
//======================================================================================================================

    override fun startThrowIfNotYet(macAddress: MacAddress, port: Int) {
        val tFile = contactCanStartThrow(macAddress)
        tFile?: return

        val throwThread: IThrowThread = when(mode) {
            ThrowModeCodes.WIFI_NETWORK -> WifiThrowThread(this, tFile, macAddress, port)
            ThrowModeCodes.BLUETOOTH    -> BtThrowThread(tFile)
            ThrowModeCodes.WIFI_DIRECT  -> return
            else -> return
        }

        activeThrowThreads[macAddress.toString()] = throwThread
        throwThread.activate()

        log.info("throwing start: " + tFile.filename + " [" + tFile.timeID + "]")
    }

    override fun startCatchIfNotYet(macAddress: MacAddress) {
        val tFile = contactCanStartCatch(macAddress)
        tFile?: return

        val catchThread: ICatchThread = when(mode) {
            ThrowModeCodes.WIFI_NETWORK -> WifiCatchThread(this, tFile)
            ThrowModeCodes.BLUETOOTH    -> BtCatchThread(this, tFile)
            ThrowModeCodes.WIFI_DIRECT  -> return
            else -> return
        }

        activeCatchThreads[macAddress.toString()] = catchThread
        catchThread.activate()

        log.info("catching start: " + tFile.filename + " [" + tFile.timeID + "]")
    }


//======================================================================================================================
// THROW FINISH
//======================================================================================================================

    override fun finishThrow(macAddress: MacAddress, timeID: Date) {
        // stop active throw and delete from active
        val throwThread = activeThrowThreads[macAddress.toString()]
        throwThread?.deactivate()
        activeThrowThreads.remove(macAddress.toString())

        // delete from queue
        val tFilesToDelete = queueFilesToThrow.filter { it.timeID == timeID }
        synchronized(queueFilesToThrow) {
            queueFilesToThrow.remove(tFilesToDelete[0])
        }
    }

    override fun finishCatch(macAddress: MacAddress, timeID: Date) {
        // stop active catch and delete from active
        val catchThread = activeCatchThreads[macAddress.toString()]
        catchThread?.deactivate()
        activeCatchThreads.remove(macAddress.toString())

        // delete throws from queue
        val tFilesToDelete = queueFilesToCatch.filter { it.timeID == timeID }
        synchronized(queueFilesToThrow) {
            queueFilesToCatch.remove(tFilesToDelete[0])
        }

        // start new catch of contact
        startCatchIfNotYet(macAddress)
    }


//======================================================================================================================
// CONTACT KICKING
//======================================================================================================================

    override fun kickUserFromThrows(macAddress: MacAddress) {
        // stop active throw and delete from active
        val throwThread = activeThrowThreads[macAddress.toString()]
        throwThread?.deactivate()
        activeCatchThreads.remove(macAddress.toString())

        // delete throws from queue
        val throwsToDelete = queueFilesToCatch.filter { it.address.compareTo(macAddress) == 0 }
        for (tFile in throwsToDelete) {
            synchronized(queueFilesToThrow) {
                queueFilesToThrow.remove(tFile)
            }
        }
    }

    override fun kickUserFromCatches(macAddress: MacAddress) {
        // stop active throw and delete from active
        val catchThread = activeCatchThreads[macAddress.toString()]
        catchThread?.deactivate()
        activeCatchThreads.remove(macAddress.toString())

        // delete throws from queue
        val throwsToDelete = queueFilesToCatch.filter { it.address.compareTo(macAddress) == 0 }
        for (tFile in throwsToDelete) {
            synchronized(queueFilesToThrow) {
                queueFilesToCatch.remove(tFile)
            }
        }
    }


//======================================================================================================================
// TRANSMITTING METHODS
//======================================================================================================================

    override fun noContact(addressee: MacAddress) {
        kickUserFromThrows(addressee)
        kickUserFromCatches(addressee)
    }

    override fun transmitThrowEndAck(endAck: ThrowEndAckObject, path: String) {
        eventManager.sendThrowEndAck(endAck, path)

        // end of catching
        finishCatch(endAck.address, endAck.timeID)
    }

    override fun getIpAddressByMac(macAddress: MacAddress): IpAddress? {
        return eventManager.getIpAddressByMac(macAddress)
    }


//======================================================================================================================
// THROWING STRATEGY
//======================================================================================================================

    private fun contactCanStartThrow(macAddress: MacAddress): ThrowFileObject? {
        // strategy of queue throwing
        if (!activeThrowThreads.containsKey(macAddress.toString())) {
            synchronized(queueFilesToThrow) {
                for (tFile in queueFilesToThrow) {
                    if (macAddress.compareTo(tFile.address) == 0) {
                        return tFile
                    }
                }
            }
        }

        return null
    }

    private fun contactCanStartCatch(macAddress: MacAddress): ThrowFileObject? {
        // strategy of queue catching
        if (!activeCatchThreads.containsKey(macAddress.toString())) {
            synchronized(queueFilesToCatch) {
                for (tFile in queueFilesToCatch) {
                    if (macAddress.compareTo(tFile.address) == 0) {
                        return tFile
                    }
                }
            }
        }

        return null
    }


}
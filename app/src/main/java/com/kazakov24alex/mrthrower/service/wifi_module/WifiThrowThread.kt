package com.kazakov24alex.mrthrower.service.wifi_module

import library.interfaces.throw_manager.IThrowManagerInformer
import library.interfaces.throw_threads.IThrowThread
import library.objects.ThrowFileObject
import library.wrappers.IpAddress
import library.wrappers.MacAddress
import java.io.OutputStream
import java.net.Socket
import java.util.*
import java.util.logging.Logger
import java.io.FileInputStream
import java.io.IOException


class WifiThrowThread(
        private val throwManager: IThrowManagerInformer,
        private val throwFile: ThrowFileObject,
        private val macAddress: MacAddress,
        private val port: Int) : Thread(), IThrowThread {

    companion object {
        val log = Logger.getLogger("["+ WifiThrowThread::class.java.name+"]")
    }


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val CHUNK_SIZE: Int = 32768


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var ipAddress: IpAddress? = null

    private var socket: Socket? = null
    private var outStream: OutputStream? = null

    private var isRunning: Boolean = false


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    override fun activate() {
        start()
    }

    override fun deactivate() {
        interrupt()
    }

    override fun getTimeID(): Date {
        return throwFile.timeID
    }


//======================================================================================================================
// Thread
//======================================================================================================================

    override fun run() {
        isRunning = true

        connectToCatcher()
        throwFile()
    }


    override fun interrupt() {
        isRunning = false

        outStream?.close()
        socket?.close()

        super.interrupt()
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private fun connectToCatcher() {
        log.info("Connecting to catching device...")

        // get host
        ipAddress = throwManager.getIpAddressByMac(macAddress)
        if(ipAddress == null) {
            log.info("catching device not found")
            throwManager.noContact(macAddress)
        }

        // set connection
        socket = Socket(ipAddress.toString(), port)

        log.info("... catching device was connected on Port=$port")
    }

    private fun throwFile() {
        log.info("WIFI-Throw process started")

        try {
            outStream = socket?.outputStream

            val fileInStream = FileInputStream(throwFile.path)

            // send file in pieces (CHUNKS)
            var bytesLeft = throwFile.fileSize
            val chunkBuffer = ByteArray(CHUNK_SIZE)

            while(bytesLeft > CHUNK_SIZE) {
                fileInStream.read(chunkBuffer)
                outStream?.write(chunkBuffer, 0, chunkBuffer.size)

                bytesLeft -= CHUNK_SIZE
            }

            // send file reminder
            val remainderBuffer = ByteArray(bytesLeft.toInt())
            fileInStream.read(remainderBuffer)
            outStream?.write(remainderBuffer, 0, remainderBuffer.size)
            outStream?.flush()

            // close file stream
            fileInStream.close()

            log.info("WIFI-Throw process finished")

        } catch (e: IOException) {
            log.warning("Sending file exception: " + e.message)
            e.printStackTrace()
        }
    }


}
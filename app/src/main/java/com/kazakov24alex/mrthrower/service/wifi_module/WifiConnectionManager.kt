package com.kazakov24alex.mrthrower.service.wifi_module

import java.util.logging.Logger
import java.net.ServerSocket
import library.wrappers.MacAddress
import library.BytePacket
import library.objects.GoodbyeObject
import library.wrappers.IpAddress
import library.interfaces.IConnectionManager
import library.interfaces.event_manager.IEventReceiver
import java.io.IOError
import java.io.IOException
import java.net.Socket


class WifiConnectionManager(
        private val eventManager: IEventReceiver) : Thread(), IConnectionManager {

    companion object {
        val log = Logger.getLogger("["+ WifiConnectionManager::class.java.name+"]")
        const val CONNECTION_PORT = 2424
    }


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val POOL_SIZE = 20


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var isRunning: Boolean = false

    private var serverSocket: ServerSocket? = null
    private var connectionPool: MutableMap<String?, WifiProtocolThread>  = mutableMapOf()  // String = MacAddress.toString()


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {

    }


//======================================================================================================================
// IConnectionManager
//======================================================================================================================
    override fun activate() {
        super.start()
    }

    override fun deactivate() {
        interrupt()
    }

    override fun specifyMacToConnection(macAddress: MacAddress) {
        val connection = connectionPool[null]

        if(connection != null) {
            connection.setMacAddress(macAddress)

            connectionPool[macAddress.toString()] = connection
            connectionPool.remove(null)

            log.info("mac address " + macAddress.toString() + " was specified for connection")
        }
    }

    override fun sendPacket(macAddress: MacAddress, packet: BytePacket) {
        connectionPool[macAddress.toString()]?.writeSocket(packet)
    }


//======================================================================================================================
// Thread // OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        super.run()
        isRunning = true

        log.info("WIFI connection manager started")

        try {
            // creating server socket on CONNECTION_PORT
            serverSocket = ServerSocket(CONNECTION_PORT, 1)
            log.info("ServerSocket opened on Port=$CONNECTION_PORT")

            while (isRunning) {
                log.info("Waiting for clients to connect...")

                // waiting client connection
                val incomingSocket = serverSocket!!.accept()

                // connection occurred
                log.info("... client accepted")

                // create a thread to process the connection
                val connection = WifiProtocolThread(this, incomingSocket, false)
                addConnection(MacAddress(), connection)
            }
        } catch (e: IOException) { }
    }

    override fun interrupt() {
        // stop server socket
        serverSocket?.close()

        // stop connection threads
        for (pair in connectionPool.entries) {
            pair.value.interrupt()
        }
        connectionPool.clear()

        isRunning = false

        log.info("WIFI connection manager stopped")
        super.interrupt()
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    fun packetReceived(macAddress: MacAddress?, bytePacket: BytePacket) {
        eventManager.onReceivedPacket(macAddress, bytePacket)
    }

    fun onDeviceFound(socket: Socket) {
        // create a thread to process the connection
        val connection = WifiProtocolThread(this, socket, true)
        addConnection(MacAddress(), connection)
    }

    private fun addConnection(macAddress: MacAddress, connection: WifiProtocolThread) {
        if (connectionPool.size < POOL_SIZE) {
            connectionPool[macAddress.toString()] = connection

            log.info("connection was added to Connection Pool")
            connection.start()

        } else {
            log.info("connection was rejected (Connection Pool is overflowed")
            connection.interrupt()
        }
    }

    fun deleteConnection(connectionThread: WifiProtocolThread) {
        connectionPool.remove(connectionThread.getMacAddress().toString())

        // emulate Goodbye packet receiving
        val goodbye = GoodbyeObject(connectionThread.getMacAddress())
        eventManager.onReceivedPacket(connectionThread.getMacAddress(), goodbye.bytePacket)

        log.info("device ["+connectionThread.getMacAddress().toString()+"] was disconnected");
    }


//======================================================================================================================
// AUXILIARY METHODS
//======================================================================================================================

    fun getIpByMac(macAddress: MacAddress): IpAddress? {
        return connectionPool[macAddress.toString()]?.getIpAddress()
    }

    override fun getActualMacAddresses(): ArrayList<MacAddress?> {
        val macs = ArrayList<MacAddress?>()
        for(connection in connectionPool) {
            macs.add(connection.value.getMacAddress())
        }

        return macs
    }
}
package com.kazakov24alex.mrthrower.service.wifi_module

import library.wrappers.MacAddress
import java.net.Socket
import java.util.logging.Logger
import java.io.*
import library.exceptions.EncodingException
import library.wrappers.IpAddress
import library.BytePacket
import library.ByteUtils
import java.net.SocketException
import library.exceptions.BuildPacketException
import com.kazakov24alex.mrthrower.database.DbManager


class WifiProtocolThread(
        private val connectionManager: WifiConnectionManager,
        private val socket: Socket,
        private val found: Boolean) : Thread() {

    companion object { val log = Logger.getLogger("["+ WifiProtocolThread::class.java.name+"]") }

//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var isRunning: Boolean = false

    private var macAddress: MacAddress? = null

    // streams
    private var dataInStream = DataInputStream(socket.getInputStream())
    private var dataOutStream = DataOutputStream(socket.getOutputStream())


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }

//======================================================================================================================
// GETTERS AND SETTERS
//======================================================================================================================

    fun setMacAddress(macAddress: MacAddress) {
        this.macAddress = macAddress
    }

    fun getMacAddress(): MacAddress? {
        return macAddress
    }

    fun getIpAddress(): IpAddress? {
        return try {
            IpAddress(socket.inetAddress?.address)
        } catch (e: EncodingException) {
            null
        }
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        isRunning = true

        if (found) {
            try {
                // send Greeting packet to remote device if it needs
                val greeting = DbManager.getProfile()
                writeSocket(greeting.bytePacket)
                log.info("Sent " + greeting.toString())
            } catch (e: BuildPacketException) { }
        }

        readSocket()
    }

    override fun interrupt() {
        isRunning = false

        try {
            dataInStream.close()
            dataOutStream.close()
            socket.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        super.interrupt()
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private fun readSocket() {
        try {
            // wait and read cycle from the socket
            var codeByte: Byte
            while (isRunning) {
                // waiting for a client message code
                codeByte = dataInStream.readByte()

                // read length of the message
                val lenBytes = ByteArray(2)
                dataInStream.read(lenBytes)

                try {
                    val length = ByteUtils.toShort(ByteUtils.bytesToObjects(lenBytes))

                    // read message
                    val message = ByteArray(length - 3)
                    dataInStream.read(message)

                    // wrap a byte array in BytePacket and transfer to ConnectionManager
                    val packet = BytePacket(codeByte, lenBytes, message)
                    connectionManager.packetReceived(macAddress, packet)

                } catch (e: EncodingException) {
                    log.warning(e.message)
                }
            }

        } catch (e: SocketException) {
            if (isRunning) {
                e.printStackTrace()
                connectionManager.deleteConnection(this)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            connectionManager.deleteConnection(this)
        }
    }

    fun writeSocket(packet: BytePacket) {
        // send message
        try {
            dataOutStream.write(packet.data)
            dataOutStream.flush()

        } catch (e: Exception) {
            log.warning("error sending protocol packet")
            e.printStackTrace()
        }
    }


}
package com.kazakov24alex.mrthrower.service

import com.kazakov24alex.mrthrower.service.interfaces.IConnectionManager
import java.util.logging.Logger
import com.kazakov24alex.mrthrower.service.interfaces.IEventReceiver
import java.net.ServerSocket
import library.wrappers.MacAddress
import java.io.IOException
import library.exceptions.EncodingException
import library.BytePacket
import library.objects.GoodbyeObject
import library.wrappers.IpAddress
import library.exceptions.BuildPacketException
import com.kazakov24alex.mrthrower.service.threads.ConnectionThread
import java.net.InetAddress
import java.net.Socket


class WifiConnectionManager(
        private val service: WifiNetworkService,
        private val port: Int) : Thread(), IConnectionManager {

    companion object { val LOG = Logger.getLogger("["+WifiConnectionManager::class.java.name+"]") }


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var isRunning: Boolean = false
    private var connectionCounter: Int = 0

    private lateinit var eventManager: IEventReceiver

    private var generalSocket: ServerSocket? = null
    private var mConnectionPool: MutableMap<String?, ConnectionThread>     // String = MacAddress.toString()


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================
    init {
        mConnectionPool = mutableMapOf<String?, ConnectionThread>()

        LOG.info("WifiConnectionManager initialized")
    }

//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        super.run()

        isRunning = true


        try {
            // creating server socket on PORT
            generalSocket = ServerSocket(port, 1)
            LOG.info("service started")
            LOG.info("ServerSocket PORT: " + port)

            while (isRunning) {
                // waiting client connection
                val incoming = generalSocket!!.accept()

                // connection occurred
                LOG.info("WifiConnectionManager client accepted")

                // create a thread to process the connection
                val connection = ConnectionThread(this)
                connection.setSocket(incoming)

                addConnection(MacAddress(), connection)
            }

        } catch (ex: IOException) {
            println("Exception : " + ex)
        }

    }

    override fun interrupt() {
        // stop server socket
        try {
            if (generalSocket != null) {
                generalSocket?.close()
                generalSocket = null
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }


        // stop connection listener threads
        for (pair in mConnectionPool.entries) {
            pair.value.interrupt()
        }
        mConnectionPool.clear()

        isRunning = false

        LOG.info("service stopped")
        super.interrupt()
    }


//======================================================================================================================
// IConnectionManager
//======================================================================================================================

    override fun setEventManager(eventManager: IEventReceiver) {
        this.eventManager = eventManager
    }

    @Throws(EncodingException::class)
    override fun specifyMacToConnection(macAddress: MacAddress) {
        val connection = mConnectionPool[null]
        connection?.setMacAddress(macAddress)
        mConnectionPool.put(macAddress.toString(), connection!!)
        mConnectionPool.remove(null)

        LOG.info("WifiConnectionManager: Mac address " + macAddress.toString() + " was specified for connection")
    }

    override fun sendPacket(macAddress: MacAddress, packet: BytePacket) {
        mConnectionPool[macAddress.toString()]?.writeSocket(packet)
    }

//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    fun addConnection(macAddress: MacAddress, connection: ConnectionThread) {
        if (mConnectionPool.size < 20) {
            mConnectionPool.put(macAddress.toString(), connection)
            connectionCounter++
            LOG.info("WifiConnectionManager: Connection was added to Connection Pool")

        } else {
            connection.interrupt()
            connectionCounter--
            LOG.info("WifiConnectionManager: Connection was rejected (Connection Pool is overflowed")
        }
    }

    fun deleteConnection(connectionThread: ConnectionThread) {
        mConnectionPool.remove(connectionThread.getMacAddress().toString())

        val goodbyeObject = GoodbyeObject(connectionThread.getMacAddress())
        val packet = goodbyeObject.bytePacket

        eventManager.onReceivedPacket(connectionThread.getMacAddress()!!, packet)
    }

    fun packetReceived(macAddress: MacAddress?, bytePacket: BytePacket) {
        eventManager.onReceivedPacket(macAddress, bytePacket)
    }

    fun sendGreeting(host: String) {
        try {
            val ipAddress = InetAddress.getByName(host)
            val socket = Socket(ipAddress, port) // port
            LOG.info(host + " device use this program")

            // create a thread to process the connection
            val connection = ConnectionThread(this)
            connection.setSocket(socket)

            addConnection(MacAddress(), connection)

            // send greeting packet on ServerSocket this IP
            val packet = service.getProfile().bytePacket
            connection.writeSocket(packet)

        } catch (e: IOException) {
            LOG.info(host + " device doesn't use this program")
        } catch (e: BuildPacketException) {
            LOG.warning("EXCEPTION: " + e.message)
            e.printStackTrace()
        }

    }


    fun getIpByMac(macAddress: MacAddress): IpAddress? {
        return mConnectionPool[macAddress.toString()]?.getIpAddress()
    }


}
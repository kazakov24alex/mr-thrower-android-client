package com.kazakov24alex.mrthrower.service.wifi_module

import com.kazakov24alex.mrthrower.service.bluetooth_module.BtCatchThread
import library.interfaces.throw_manager.IThrowManagerInformer
import library.interfaces.throw_threads.ICatchThread
import library.objects.ThrowFileObject
import java.io.DataInputStream
import java.io.FileOutputStream
import java.net.ServerSocket
import java.net.Socket
import java.util.logging.Logger
import library.objects.ThrowEndAckObject
import library.objects.ThrowProgressObject
import library.objects.ThrowResponseObject
import org.greenrobot.eventbus.EventBus
import java.io.IOException
import library.wrappers.EventWrapper


class WifiCatchThread(
        private val throwManager: IThrowManagerInformer,
        private val catchFile: ThrowFileObject) : Thread(), ICatchThread {

    companion object { val log = Logger.getLogger("["+ WifiCatchThread::class.java.name+"]") }


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val CHUNK_SIZE = 32768
    private val PROGRESS_UPDATE_LIMIT = 100 * 1024  // bytes


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var serverSocket: ServerSocket? = null
    private var incomingSocket: Socket? = null
    private var dataInputStream: DataInputStream? = null
    private var fileOutputStream: FileOutputStream? = null

    private var isCompleted: Boolean = false


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    override fun activate() {
        start()
    }

    override fun deactivate() {
        interrupt()
    }

//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        connectToThrower()
        catchFile()
    }

    override fun interrupt() {
        fileOutputStream?.close()
        dataInputStream?.close()
        incomingSocket?.close()
        serverSocket?.close()

        if(!isCompleted)
            // TODO delete file

        super.interrupt()
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private fun connectToThrower() {
        // create server socket on available port
        serverSocket = ServerSocket(0)

        // send ThrowResponse with file TimeID and CONNECTION_PORT
        val response = ThrowResponseObject(catchFile.timeID, serverSocket!!.localPort)
        response.address = catchFile.address
        EventBus.getDefault().post(EventWrapper(response, EventWrapper.TO_EVENT_MANAGER))

        // connection waiting
        log.info("Waiting for thrower to connect ...")
        incomingSocket = serverSocket!!.accept()
        log.info("... thrower connected on Port=" + incomingSocket!!.localPort)
    }

    private fun catchFile() {
        log.info("WIFI-Catch process started")

        try {
            dataInputStream = DataInputStream(incomingSocket!!.inputStream)

            // limit of progress dispatch
            var progressLimit = PROGRESS_UPDATE_LIMIT

            var bytesReceived: Long = 0

            //val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
            val path = "/storage/emulated/0/Download/" + this.catchFile.filename
            fileOutputStream = FileOutputStream(path)

            while (true) {
                // read in pieces
                val chunk = ByteArray(CHUNK_SIZE)
                val readBytesCount = dataInputStream!!.read(chunk)

                if (readBytesCount > 0) {
                    fileOutputStream?.write(chunk, 0, readBytesCount)

                    bytesReceived += readBytesCount.toLong()

                    // send progress
                    if(bytesReceived > progressLimit) {
                        val progress = ThrowProgressObject(this.catchFile.timeID, bytesReceived)
                        progress.address = catchFile.address
                        EventBus.getDefault().post(EventWrapper(progress, EventWrapper.TO_EVENT_MANAGER))

                        progressLimit += PROGRESS_UPDATE_LIMIT
                    }

                    if(bytesReceived == catchFile.fileSize)
                        break
                }
            }

            // close file
            fileOutputStream?.flush()
            fileOutputStream?.close()
            fileOutputStream = null

            isCompleted = (bytesReceived == catchFile.fileSize)

            // send end ack notification
            val endAck = ThrowEndAckObject(this.catchFile.timeID, isCompleted)
            endAck.address = catchFile.address
            throwManager.transmitThrowEndAck(endAck, path)

        } catch (e: IOException) {
            e.printStackTrace()
        }

        log.info("WIFI-Catch process finished")
    }


}
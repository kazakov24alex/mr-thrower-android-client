package com.kazakov24alex.mrthrower.service.bluetooth_module

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.kazakov24alex.mrthrower.MrThrowerApp
import library.interfaces.IDeviceResearcher
import java.util.*
import java.util.logging.Logger


class BtDeviceResearchThread(
        private val deviceFinder: IBtDeviceFinder,
        private val uuid: UUID) : Thread(), IDeviceResearcher {

    companion object { val log = Logger.getLogger("["+ BtDeviceResearchThread::class.java.name+"]") }


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val TIME_TO_SEARCH: Long = 10000 // ms


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private val btAdapter = BluetoothAdapter.getDefaultAdapter()

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (BluetoothDevice.ACTION_FOUND == intent.action) {
                // extract data of found remote device
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

                // connect to find remote device on UUID
                val btSocket = device.createRfcommSocketToServiceRecord(uuid)
                try { btSocket.connect() } catch(e: Exception) { }

                // check the device support for this url
                if(btSocket.isConnected) {
                    log.info("device [" + device.name + ", " + device.address + "] found")
                    deviceFinder.onDeviceFound(btSocket)

                } else {
                    log.info("device [" + device.name + ", " + device.address +  "] doesn't use this program")
                }
            }
        }
    }


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        // subscribing
        MrThrowerApp.instance.registerReceiver(mReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))

        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// IDeviceResearcher
//======================================================================================================================

    override fun activate() {
        start()
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        super.run()

        log.info("Bluetooth device finder search started")
        btAdapter.startDiscovery()

        sleep(TIME_TO_SEARCH)

        btAdapter.cancelDiscovery()
        log.info("Bluetooth device finder search finished")

        interrupt()
    }


    override fun interrupt() {
        super.interrupt()

        // unsubscribing
        MrThrowerApp.instance.unregisterReceiver(mReceiver)
    }


}
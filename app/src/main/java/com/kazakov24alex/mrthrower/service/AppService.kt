package com.kazakov24alex.mrthrower.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.kazakov24alex.mrthrower.gui.activities.WelcomeActivity
import java.util.logging.Logger

class AppService : Service() {

    companion object { val log = Logger.getLogger(AppService::class.java.name) }


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var mode: Byte = -1
    private var isRunning: Boolean = false

    private lateinit var eventManager: EventManager


//======================================================================================================================
// SERVICE
//======================================================================================================================

    override fun onBind(intent: Intent): IBinder? {
        log.info("onBind")
        throw UnsupportedOperationException("Not yet implemented")
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        mode = intent!!.getByteExtra(WelcomeActivity.INTENT_EXTRA_MODE,-1)

        isRunning = true
        log.info("APP SERVICE started")

        eventManager = EventManager(mode)

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        if(isRunning) {
            isRunning = false
            eventManager.stop()
        }

        log.info("service was destroyed")
        super.onDestroy()
    }


}

package com.kazakov24alex.mrthrower.service.bluetooth_module

import android.bluetooth.BluetoothSocket

interface IBtDeviceFinder {

    fun onDeviceFound(btSocket: BluetoothSocket)

}
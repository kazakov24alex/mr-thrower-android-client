package com.kazakov24alex.mrthrower.service.bluetooth_module

import android.bluetooth.BluetoothSocket
import com.kazakov24alex.mrthrower.database.DbManager
import library.wrappers.MacAddress
import java.util.logging.Logger
import java.io.*
import library.exceptions.EncodingException
import library.BytePacket
import library.ByteUtils
import library.exceptions.BuildPacketException
import java.net.SocketException


class BtProtocolThread(
        private val wifiConnectionManager: BtConnectionManager,
        private val socket: BluetoothSocket,
        private val found: Boolean) : Thread() {

    companion object { val log = Logger.getLogger("["+ BtProtocolThread::class.java.name+"]") }

//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var macAddress: MacAddress? = null
    private var isRunning: Boolean = false

    // streams
    private val dataInStream: DataInputStream = DataInputStream(socket.inputStream)
    private val dataOutStream: DataOutputStream = DataOutputStream(socket.outputStream)


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    fun getMacAddress(): MacAddress? {
        return macAddress
    }

    fun setMacAddress(macAddress: MacAddress) {
        this.macAddress = macAddress
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        isRunning = true

        // send Greeting packet to remote device if it needs
        if(!found) {
            try {
                val greeting = DbManager.getProfile()
                writeSocket(greeting.bytePacket)
                log.info("Sent " + greeting.toString())
            } catch (e: BuildPacketException) { }
        }

        readSocket()
    }

    override fun interrupt() {
        isRunning = false

        try {
            dataInStream.close()
            dataOutStream.close()
            socket.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }

        super.interrupt()
    }


//======================================================================================================================
// READ AND WRITE SOCKET
//======================================================================================================================

    private fun readSocket() {
        try {
            // wait and read cycle from the socket
            var codeByte: Byte
            while (isRunning) {
                // waiting for a client message code
                codeByte = dataInStream.readByte()

                // read length of the message
                val lenBytes = ByteArray(2)
                dataInStream.read(lenBytes)

                try {
                    val length = ByteUtils.toShort(ByteUtils.bytesToObjects(lenBytes))

                    // read message
                    val message = ByteArray(length - 3)
                    dataInStream.read(message)

                    // wrap a byte array in BytePacket and transfer to ConnectionManager
                    val packet = BytePacket(codeByte, lenBytes, message)
                    wifiConnectionManager.onPacketReceived(macAddress, packet)

                } catch (e: EncodingException) {
                    log.warning(e.message)
                }
            }

        } catch (e: SocketException) {
            if (isRunning) {
                e.printStackTrace()
                wifiConnectionManager.deleteConnection(this)
            }
        } catch (e: IOException) {
            wifiConnectionManager.deleteConnection(this)
        }

    }

    fun writeSocket(packet: BytePacket) {
        try {
            // send message
            dataOutStream.write(packet.data)
            dataOutStream.flush()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}
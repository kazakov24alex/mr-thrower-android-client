package com.kazakov24alex.mrthrower.service

import com.kazakov24alex.mrthrower.MrThrowerApp
import com.kazakov24alex.mrthrower.database.DbManager
import com.kazakov24alex.mrthrower.utils.experts.WifiNetworkExpert
import com.kazakov24alex.mrthrower.service.bluetooth_module.BtConnectionManager
import com.kazakov24alex.mrthrower.service.bluetooth_module.BtDeviceResearchThread
import com.kazakov24alex.mrthrower.service.wifi_module.WifiConnectionManager
import com.kazakov24alex.mrthrower.service.wifi_module.WifiDeviceResearchThread

import library.BytePacket
import library.codes.DirectionCodes
import library.wrappers.MacAddress
import library.exceptions.EncodingException
import library.exceptions.BuildPacketException
import library.exceptions.ParsePacketException
import library.codes.PacketCodes
import library.codes.ThrowModeCodes
import library.codes.ThrowStateCodes
import library.interfaces.IConnectionManager
import library.interfaces.event_manager.IEventCreator
import library.interfaces.event_manager.IEventReceiver
import library.interfaces.event_manager.IEventThrow
import library.interfaces.throw_manager.IThrowManager

import library.objects.GreetingObject
import library.objects.GoodbyeObject
import library.objects.MessageAckObject
import library.objects.MessageObject
import library.objects.ThrowResponseObject
import library.objects.ThrowRequestObject
import library.objects.ThrowProgressObject
import library.objects.ThrowEndAckObject
import library.wrappers.EventWrapper
import library.wrappers.IpAddress

import java.util.logging.Logger
import java.util.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class EventManager(
        private val mode: Byte) : IEventReceiver, IEventCreator, IEventThrow {

    companion object {
        val log = Logger.getLogger("["+EventManager::class.java.name+"]")
    }


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val WIFI_ADDRESS_RANGE = 255


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var throwManager: IThrowManager = ThrowManager(this, mode)
    private var connectionManager: IConnectionManager? = null


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        EventBus.getDefault().register(this)

        when(mode) {
            ThrowModeCodes.WIFI_NETWORK ->  connectionManager = WifiConnectionManager(this)
            ThrowModeCodes.BLUETOOTH ->     connectionManager = BtConnectionManager(this)
            ThrowModeCodes.WIFI_DIRECT ->   connectionManager = null
        }
        connectionManager?.activate()

        when(mode) {
            ThrowModeCodes.WIFI_NETWORK ->  startWifiDeviceResearch()
            ThrowModeCodes.BLUETOOTH ->     startBluetoothDeviceResearch()
            ThrowModeCodes.WIFI_DIRECT ->   startDirectDeviceResearch()
        }

        log.info("event manager initialized")
    }

    fun stop() {
        throwManager.stop()
        connectionManager?.deactivate()

        EventBus.getDefault().unregister(this)
        log.info("event manager stopped")
    }


//======================================================================================================================
// DEVICE SEARCH
//======================================================================================================================

    private fun startWifiDeviceResearch() {
        val currentIP = WifiNetworkExpert.getIpAddress(MrThrowerApp.instance).toString()

        // getting subnet in String format
        val firstSeparator = currentIP.lastIndexOf("/")
        val lastSeparator = currentIP.lastIndexOf(".")
        val subnet = currentIP.substring(firstSeparator + 1, lastSeparator + 1)

        // ping all the addresses of this network
        for (i in 1..WIFI_ADDRESS_RANGE) {
            val host = subnet + i

            // exclude checking own IP address
            if (host == currentIP)
                continue

            WifiDeviceResearchThread(host, connectionManager as WifiConnectionManager).start()
        }
    }

    private fun startBluetoothDeviceResearch() {
        BtDeviceResearchThread(connectionManager as BtConnectionManager, BtConnectionManager.APP_UUID).start()
    }

    private fun startDirectDeviceResearch() {
        val contacts = DbManager.selectAllContacts()
        for (contact in contacts) {
            onReceivedGreeting(contact.bytePacket)
        }

    }


//======================================================================================================================
// UTILS
//======================================================================================================================

    override fun getIpAddressByMac(macAddress: MacAddress): IpAddress? {
        return (connectionManager as WifiConnectionManager).getIpByMac(macAddress)
    }


//======================================================================================================================
// IEventReceiver // EVENT FROM CONNECTIONS
//======================================================================================================================

    // PACKET IDENTIFIER
    override fun onReceivedPacket(sender: MacAddress?, bytePacket: BytePacket) {
        try {
            when (bytePacket.getByte(0)) {
                PacketCodes.GREETING ->         onReceivedGreeting(bytePacket) // sender is still null !!!
                PacketCodes.GREETING_ACK ->     onReceivedGreetingAck(bytePacket)
                PacketCodes.GOODBYE ->          onReceivedGoodbye(bytePacket)
                PacketCodes.MESSAGE ->          onReceivedMessage(sender!!, bytePacket)
                PacketCodes.MESSAGE_ACK ->      onReceivedMessageAck(sender!!, bytePacket)
                PacketCodes.THROW_REQUEST ->    onReceivedThrowRequest(sender!!, bytePacket)
                PacketCodes.THROW_RESPONSE ->   onReceivedThrowResponse(sender!!, bytePacket)
                PacketCodes.THROW_PROGRESS ->   onReceivedThrowProgress(sender!!, bytePacket)
                PacketCodes.THROW_END_ACK ->    onReceivedThrowEndAck(sender!!, bytePacket)
            }

        } catch (e: EncodingException) {
            // incorrect packet
            e.printStackTrace()
        }
    }


//======================================================================================================================
// PACKET HANDLERS
//======================================================================================================================

    override fun onReceivedGreeting(packet: BytePacket) {
        try {
            // parse packet to Greeting object
            val greeting = GreetingObject(packet)
            greeting.address = greeting.macAddress
            log.info("Received " + greeting.toString())

            connectionManager?.specifyMacToConnection(greeting.macAddress)

            // add to database
            DbManager.insertContact(greeting)
            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(greeting, EventWrapper.FROM_EVENT_MANAGER))

            // sent reciprocal greeting
            val greetingAck = DbManager.getProfile()
            greetingAck.address = greeting.macAddress
            sendGreetingAck(greetingAck)

        } catch (e: Exception) {
            log.warning("EXCEPTION: Receiving [GREETING]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun onReceivedGreetingAck(packet: BytePacket) {
        try {
            // parse packet to Greeting object
            val greetingAck = GreetingObject(packet)
            greetingAck.address = greetingAck.macAddress
            log.info("Received " + greetingAck.toString())

            connectionManager?.specifyMacToConnection(greetingAck.macAddress)

            // add to database
            DbManager.insertContact(greetingAck)
            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(greetingAck, EventWrapper.FROM_EVENT_MANAGER))

        } catch (e: Exception) {
            log.warning("EXCEPTION: Receiving [GREETING ACK]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun onReceivedGoodbye(packet: BytePacket) {
        try {
            // parse packet to Greeting object
            val goodbye = GoodbyeObject(packet)
            log.info("Received " + goodbye.toString())

            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(goodbye, EventWrapper.FROM_EVENT_MANAGER))

        } catch (e: Exception) {
            log.warning("EXCEPTION: Receiving [GOODBYE ACK]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun onReceivedMessage(sender: MacAddress, packet: BytePacket) {
        try {
            // parse packet to Message object
            val message = MessageObject(packet, sender)
            log.info("Received " + message.toString())

            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(message, EventWrapper.FROM_EVENT_MANAGER))
            // add to database
            DbManager.insertMessage(message, mode, ThrowStateCodes.STATE_PROCESS, DirectionCodes.INCOMING)

            // send acknowledgment to sender
            val messageAck = MessageAckObject(message.timeID, MessageAckObject.CODE_SUCCESS, null)
            messageAck.address = sender
            sendMessageAck(messageAck)

        } catch (e: ParsePacketException) {
            log.warning("EXCEPTION: Receiving [MESSAGE] from [" + sender.toString() + "]\n" + e.message)
            e.printStackTrace()

            // send acknowledgment to sender
            val ack = MessageAckObject(Date(), MessageAckObject.CODE_FAILURE, "ParsePacketException")
            ack.address = sender
            sendMessageAck(ack)

        }

    }

    override fun onReceivedMessageAck(sender: MacAddress, packet: BytePacket) {
        try {
            // parse packet to Message object
            val messageAck = MessageAckObject(packet, sender)
            log.info("Received " + messageAck.toString())

            // add to database
            DbManager.updateMessage(sender, messageAck.timeID, ThrowStateCodes.STATE_FINISHED)
            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(messageAck, EventWrapper.FROM_EVENT_MANAGER))

        } catch (e: ParsePacketException) {
            log.warning("EXCEPTION: Receiving [MESSAGE ACK] from [" + sender.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun onReceivedThrowRequest(sender: MacAddress, packet: BytePacket) {
        try {
            // parse packet to ThrowRequest object
            val request = ThrowRequestObject(packet, sender)
            log.info("Received " + request.toString())

            // add to database
            for(tFile in request.throwFiles) {
                DbManager.insertFileThrow(tFile, mode, ThrowStateCodes.STATE_REQUEST, DirectionCodes.INCOMING)
            }
            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(request, EventWrapper.FROM_EVENT_MANAGER))

        } catch (e: ParsePacketException) {
            log.warning("EXCEPTION: Receiving [THROW REQUEST] from [" + sender.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun onReceivedThrowResponse(sender: MacAddress, packet: BytePacket) {
        try {
            // parse packet to ThrowProgress object
            val response = ThrowResponseObject(packet, sender)
            log.info("Received " + response.toString())

            // add to database
            if(response.isNegative) {
                DbManager.updateThrowState(sender, response.timeID, ThrowStateCodes.STATE_REJECTED)
            } else {
                DbManager.updateThrowState(sender, response.timeID, ThrowStateCodes.STATE_PROCESS)
            }

            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(response, EventWrapper.FROM_EVENT_MANAGER))

            throwManager.startThrowIfNotYet(sender, response.port)

        } catch (e: ParsePacketException) {
            log.warning("EXCEPTION: Receiving [THROW RESPONSE] from [" + sender.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun onReceivedThrowProgress(sender: MacAddress, packet: BytePacket) {
        try {
            // parse packet to ThrowProgress object
            val progress = ThrowProgressObject(packet, sender)
            log.info("Received " + progress.toString())

            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(progress, EventWrapper.FROM_EVENT_MANAGER))

        } catch (e: ParsePacketException) {
            log.warning("EXCEPTION: Receiving [THROW PROGRESS] from [" + sender.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun onReceivedThrowEndAck(sender: MacAddress, packet: BytePacket) {
        try {
            // parse packet to ThrowProgress object
            val endAck = ThrowEndAckObject(packet, sender)
            log.info("Received " + endAck.toString())

            // finish this throw
            throwManager.finishThrow(sender, endAck.timeID)

            // add to database
            when(endAck.isSuccess) {
                true ->  DbManager.updateThrowState(sender, endAck.timeID, ThrowStateCodes.STATE_FINISHED)
                false -> DbManager.updateThrowState(sender, endAck.timeID, ThrowStateCodes.STATE_INTERRUPTED)
            }

            // fire an event to listeners
            EventBus.getDefault().post(EventWrapper(endAck, EventWrapper.FROM_EVENT_MANAGER))

        } catch (e: ParsePacketException) {
            log.warning("EXCEPTION: Receiving [THROW PROGRESS] from [" + sender.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }


//======================================================================================================================
// IEventCreator // EVENTS FROM GUI
//======================================================================================================================

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun sendEvent(event: EventWrapper) {
        if(event.direction == EventWrapper.TO_EVENT_MANAGER) {
            when(event.code) {
                PacketCodes.GREETING ->         sendGreetingAck(event.`object` as GreetingObject)
                PacketCodes.MESSAGE ->          sendMessage(event.`object` as MessageObject)
                PacketCodes.MESSAGE_ACK ->      sendMessageAck(event.`object` as MessageAckObject)
                PacketCodes.THROW_REQUEST ->    sendThrowRequest(event.`object` as ThrowRequestObject)
                PacketCodes.THROW_RESPONSE ->   sendThrowResponse(event.`object` as ThrowResponseObject)
                PacketCodes.THROW_PROGRESS ->   sendThrowProgress(event.`object` as ThrowProgressObject)
                //PacketCodes.THROW_END_ACK ->  sendThrowEndAck(event.getObject() as ThrowEndAckObject)

                // internal queries
                PacketCodes.REQUEST_CONTACTS -> getActualContacts()
            }
        }

        if(event.direction == EventWrapper.TO_THROW_MANAGER) {
            when(event.code) {
                PacketCodes.THROW_REQUEST -> catchThrowRequest(event.`object` as ThrowRequestObject)
            }
        }
    }


//======================================================================================================================
// IEventCreator // SENDERS
//======================================================================================================================

    override fun sendGreetingAck(greeting: GreetingObject) {
        try {
            // send packet
            connectionManager?.sendPacket(greeting.address, greeting.ackBytePacket)
            log.info("Sent " + greeting.toString())

        } catch (e: Exception) {
            log.warning("EXCEPTION: Sending [GREETING ACK] to [" + greeting.address.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun sendMessage(message: MessageObject) {
        try {
            // send packet
            connectionManager?.sendPacket(message.address, message.bytePacket)
            log.info("Sent " + message.toString())

            // write to database
            DbManager.insertMessage(message, mode, ThrowStateCodes.STATE_PROCESS, DirectionCodes.OUTCOMING)

        } catch (e: Exception) {
            log.warning("EXCEPTION: Sending [MESSAGE] to [" + message.address.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun sendMessageAck(messageAck: MessageAckObject) {
        try {
            // send packet
            connectionManager?.sendPacket(messageAck.address, messageAck.bytePacket)
            log.info("Sent " + messageAck.toString())

            // write to database
            DbManager.updateMessage(messageAck.address, messageAck.timeID, ThrowStateCodes.STATE_FINISHED)

        } catch (e: Exception) {
            log.warning("EXCEPTION: Sending [MESSAGE ACK] to [" + messageAck.address.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun sendThrowRequest(request: ThrowRequestObject) {
        try {
            // add to throw queue
            throwManager.addThrowFiles(request.throwFiles)

            // send packet
            connectionManager?.sendPacket(request.address, request.bytePacket)
            log.info("Sent " + request.toString())

            // write to database
            for(tFile in request.throwFiles) {
                DbManager.insertFileThrow(tFile, mode, ThrowStateCodes.STATE_REQUEST, DirectionCodes.OUTCOMING)
            }

        } catch (e: BuildPacketException) {
            log.warning("EXCEPTION: Sending [THROW REQUEST] to [" + request.address.toString() + "]\n" + e.message)
            e.printStackTrace()
        }

    }

    override fun catchThrowRequest(request: ThrowRequestObject) {
        throwManager.addCatchFiles(request.throwFiles)
        throwManager.startCatchIfNotYet(request.address)
    }

    override fun getActualContacts() {
        if(mode == ThrowModeCodes.WIFI_DIRECT) {
            startDirectDeviceResearch()
            return
        }

        val macs = connectionManager!!.actualMacAddresses
        for(mac in macs) {
            val greeting = DbManager.selectContactByMac(mac)
            EventBus.getDefault().post(EventWrapper(greeting, EventWrapper.FROM_EVENT_MANAGER))
        }
    }


//======================================================================================================================
// IEventThrower // EVENTS FROM THROW MANAGER
//======================================================================================================================

    override fun sendThrowResponse(response: ThrowResponseObject) {
        // send packet
        connectionManager?.sendPacket(response.address, response.bytePacket)
        log.info("Sent " + response.toString())

        // write to database
        when (response.isNegative) {
            true ->  DbManager.updateThrowState(response.address, response.timeID, ThrowStateCodes.STATE_REJECTED)
            false -> DbManager.updateThrowState(response.address, response.timeID, ThrowStateCodes.STATE_PROCESS)
        }

        // fire an event to listeners
        EventBus.getDefault().post(EventWrapper(response, EventWrapper.FROM_EVENT_MANAGER))
    }

    override fun sendThrowProgress(progress: ThrowProgressObject) {
        // send packet
        connectionManager?.sendPacket(progress.address, progress.bytePacket)
        log.info("Sent " + progress.toString())

        // fire an event to listeners
        EventBus.getDefault().post(EventWrapper(progress, EventWrapper.FROM_EVENT_MANAGER))
    }

    override fun sendThrowEndAck(endAck: ThrowEndAckObject, path: String) {
        // send packet
        connectionManager?.sendPacket(endAck.address, endAck.bytePacket)
        log.info("Sent " + endAck.toString())

        // write to database
        if(endAck.isSuccess) {
            DbManager.updateThrowState(endAck.address, endAck.timeID, ThrowStateCodes.STATE_FINISHED)
            DbManager.updateThrowPath(endAck.address, endAck.timeID, path)
        } else {
            DbManager.updateThrowState(endAck.address, endAck.timeID, ThrowStateCodes.STATE_INTERRUPTED)
        }

        // fire an event to listeners
        EventBus.getDefault().post(EventWrapper(endAck, EventWrapper.FROM_EVENT_MANAGER))
    }


}
package com.kazakov24alex.mrthrower.service.bluetooth_module

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import library.interfaces.throw_manager.IThrowManagerInformer
import library.interfaces.throw_threads.ICatchThread
import library.objects.ThrowEndAckObject
import library.objects.ThrowFileObject
import library.objects.ThrowProgressObject
import library.objects.ThrowResponseObject
import library.wrappers.EventWrapper
import org.greenrobot.eventbus.EventBus
import java.io.DataInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import java.util.logging.Logger


class BtCatchThread(
        private val throwManager: IThrowManagerInformer,
        private val catchFile: ThrowFileObject) : Thread(), ICatchThread {

    companion object { val log = Logger.getLogger("["+ BtCatchThread::class.java.name+"]") }

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val CHUNK_SIZE = 32768
    private val THROW_UUID = UUID.fromString(catchFile.uuiDstart + "-0000-1000-8000-00805F9B34FB")
    private val PROGRESS_UPDATE_LIMIT = 100 * 1024  // bytes


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var bluetoothAdapter: BluetoothAdapter? = null
    private var serverSocket: BluetoothServerSocket? = null

    private var incomingSocket: BluetoothSocket? = null
    private var dataInputStream: DataInputStream? = null
    private var fileOutputStream: FileOutputStream? = null

    private var isCompleted: Boolean = false


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    init {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    override fun activate() {
        start()
    }

    override fun deactivate() {
        interrupt()
    }

//======================================================================================================================
// OPERATING CYCLE  // Thread
//======================================================================================================================

    override fun run() {
        log.info("thread started")

        connectToThrower()
        catchFile()
    }

    override fun interrupt() {
        fileOutputStream?.close()
        dataInputStream?.close()
        incomingSocket?.close()
        serverSocket?.close()

        if(!isCompleted) {
            // TODO delete file
        }

        log.info("thread finished")
        super.interrupt()
    }

//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private fun connectToThrower() {
        // create server socket on certain URL
        serverSocket = bluetoothAdapter!!.listenUsingRfcommWithServiceRecord("MrThrower", THROW_UUID)
        log.info("Waiting for thrower to connect ...")
        log.info("ServerSocket opened on UUID = ${THROW_UUID}")

        // send ThrowResponse with file TimeID
        val response = ThrowResponseObject(catchFile.timeID, 0)
        response.address = catchFile.address
        EventBus.getDefault().post(EventWrapper(response, EventWrapper.TO_EVENT_MANAGER))

        // waiting for thrower
        incomingSocket = serverSocket!!.accept()

        // if connection is not established
        if (incomingSocket == null) {
            val endAck = ThrowEndAckObject(this.catchFile.timeID, false)
            endAck.address = catchFile.address
            throwManager.transmitThrowEndAck(endAck, null)
            return
        }

        log.info("... thrower connected")
    }

    private fun catchFile() {
        log.info("BT-Catch process started")

        try {
            dataInputStream = DataInputStream(incomingSocket!!.inputStream)

            // limit of progress dispatch
            var progressLimit = PROGRESS_UPDATE_LIMIT

            var bytesReceived: Long = 0

            //val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
            val path = "/storage/emulated/0/Download/" + this.catchFile.filename
            fileOutputStream = FileOutputStream(path)

            while (true) {
                // read in pieces
                val chunk = ByteArray(CHUNK_SIZE)
                val readBytesCount = dataInputStream!!.read(chunk)

                if (readBytesCount > 0) {
                    fileOutputStream?.write(chunk, 0, readBytesCount)

                    bytesReceived += readBytesCount.toLong()

                    // send progress
                    if(bytesReceived > progressLimit) {
                        val progress = ThrowProgressObject(this.catchFile.timeID, bytesReceived)
                        progress.address = catchFile.address
                        EventBus.getDefault().post(EventWrapper(progress, EventWrapper.TO_EVENT_MANAGER))

                        progressLimit += PROGRESS_UPDATE_LIMIT
                    }

                    if(bytesReceived == catchFile.fileSize)
                        break
                }
            }

            // close file
            fileOutputStream?.flush()
            fileOutputStream?.close()
            fileOutputStream = null

            isCompleted = (bytesReceived == catchFile.fileSize)

            // send end ack notification
            val endAck = ThrowEndAckObject(this.catchFile.timeID, isCompleted)
            endAck.address = catchFile.address
            throwManager.transmitThrowEndAck(endAck, path)

        } catch (e: IOException) {
            e.printStackTrace()
        }

        log.info("BT-Catch process finished")
    }


}
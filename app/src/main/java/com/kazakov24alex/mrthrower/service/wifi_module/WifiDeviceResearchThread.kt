package com.kazakov24alex.mrthrower.service.wifi_module

import library.interfaces.IDeviceResearcher
import java.io.IOException
import java.util.logging.Logger
import java.net.InetAddress
import java.net.Socket


class WifiDeviceResearchThread(
        private val host: String,
        private val wifiConnectionManager: WifiConnectionManager) : Thread(), IDeviceResearcher {

    companion object { val log = Logger.getLogger("["+ WifiDeviceResearchThread::class.java.name+"]") }


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val PING_TIMEOUT = 1000 // ms
    private val CONNECTION_PORT = WifiConnectionManager.CONNECTION_PORT


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    init {
        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// IDeviceResearcher
//======================================================================================================================

    override fun activate() {
        start()
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        super.run()

        try {
            // ping this IP address
            if (InetAddress.getByName(host).isReachable(PING_TIMEOUT)) {
                log.info("$host is reachable")

                // try to connect on CONNECTION_PORT
                val ipAddress = InetAddress.getByName(host)
                val socket = Socket(ipAddress, CONNECTION_PORT)
                log.info("$host device use this program")

                wifiConnectionManager.onDeviceFound(socket)
            }

        } catch (e: IOException) {
            log.info("$host device doesn't use this program")
        }

        interrupt()
    }



}
package com.kazakov24alex.mrthrower.service.bluetooth_module

import android.bluetooth.BluetoothAdapter
import library.BytePacket
import library.interfaces.IConnectionManager
import library.interfaces.event_manager.IEventReceiver
import library.wrappers.MacAddress
import java.util.logging.Logger
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import library.exceptions.EncodingException
import library.objects.GoodbyeObject
import java.io.IOException
import java.util.*


class BtConnectionManager(
        private val eventManager: IEventReceiver) : Thread(), IConnectionManager, IBtDeviceFinder {

    companion object {
        val log = Logger.getLogger("[" + BtConnectionManager::class.java.name + "]")

        val APP_NAME = "MrThrower"
        val APP_UUID = UUID.fromString("00000000-0000-1000-8000-00805F9B34FB")
    }

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val POOL_SIZE = 20


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var isRunning: Boolean = false

    private var bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private var serverSocket: BluetoothServerSocket? = null
    private var connectionPool: MutableMap<String?, BtProtocolThread> = mutableMapOf() // String = MacAddress.toString()


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================
    init {
        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// IConnectionManager
//======================================================================================================================
    override fun activate() {
        super.start()
    }

    override fun deactivate() {
        interrupt()
    }

    @Throws(EncodingException::class)
    override fun specifyMacToConnection(macAddress: MacAddress) {
        val connection = connectionPool[null]

        if(connection != null) {
            connection.setMacAddress(macAddress)

            connectionPool[macAddress.toString()] = connection
            connectionPool.remove(null)

            log.info("mac address " + macAddress.toString() + " was specified for connection")
        }
    }

    override fun sendPacket(macAddress: MacAddress, packet: BytePacket) {
        connectionPool[macAddress.toString()]?.writeSocket(packet)
    }


//======================================================================================================================
// Thread // OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        super.run()
        isRunning = true
        log.info("BT connection manager started")

        while (isRunning) {
            try {
                // creating server socket on APP_UUID
                serverSocket = bluetoothAdapter!!.listenUsingRfcommWithServiceRecord(APP_NAME, APP_UUID)
                log.info("ServerSocket opened on UUID = $APP_UUID")

                log.info("Waiting for clients to connect...")

                // waiting client connection
                val socket = serverSocket!!.accept()

                // check if the connection is confirmed
                if (socket != null) {
                    // connection occurred
                    log.info("... client accepted")

                    // create a thread to process the connection
                    val btThread = BtProtocolThread(this, socket, false)
                    addConnection(MacAddress(), btThread)
                }
            } catch (e: IOException) { }
        }
    }

    override fun interrupt() {
        // stop server socket
        log.info("SOOOOTEOTG")
        try {
            serverSocket?.close()
        } catch (e: Exception) {}

        // stop connection threads
        for (pair in connectionPool.entries) {
            pair.value.interrupt()
        }
        connectionPool.clear()

        isRunning = false

        log.info("BT connection manager stopped")
        super.interrupt()

    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    fun onPacketReceived(macAddress: MacAddress?, bytePacket: BytePacket) {
        eventManager.onReceivedPacket(macAddress, bytePacket)
    }

    override fun onDeviceFound(btSocket: BluetoothSocket) {
        val btThread = BtProtocolThread(this, btSocket, true)
        addConnection(MacAddress(), btThread)
    }

    private fun addConnection(macAddress: MacAddress, connection: BtProtocolThread) {
        if (connectionPool.size < POOL_SIZE) {
            connectionPool[macAddress.toString()] = connection

            log.info("connection was added to Connection Pool");
            connection.start()

        } else {
            log.info("connection was rejected (Connection Pool is overflowed");
            connection.interrupt()
        }
    }

    fun deleteConnection(connectionThread: BtProtocolThread) {
        connectionPool.remove(connectionThread.getMacAddress().toString())

        // emulate Goodbye packet receiving
        val goodbyeObject = GoodbyeObject(connectionThread.getMacAddress())
        eventManager.onReceivedPacket(connectionThread.getMacAddress(), goodbyeObject.bytePacket)

        log.info("device ["+connectionThread.getMacAddress().toString()+"] was disconnected");
    }


//======================================================================================================================
// AUXILIARY METHODS
//======================================================================================================================

    override fun getActualMacAddresses(): ArrayList<MacAddress?> {
        val macs = ArrayList<MacAddress?>()
        for(connection in connectionPool) {
            macs.add(connection.value.getMacAddress())
        }

        return macs
    }

}
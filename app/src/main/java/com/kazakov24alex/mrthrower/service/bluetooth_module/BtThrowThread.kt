package com.kazakov24alex.mrthrower.service.bluetooth_module

import android.bluetooth.BluetoothSocket
import library.interfaces.throw_threads.IThrowThread
import library.objects.ThrowFileObject
import java.io.FileInputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*
import java.util.logging.Logger


class BtThrowThread(
        private val throwFile: ThrowFileObject) : Thread(), IThrowThread, IBtDeviceFinder {

    companion object {
        val log = Logger.getLogger("["+ BtThrowThread::class.java.name+"]")
    }

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private val CHUNK_SIZE: Int = 32768
    private val THROW_UUID = UUID.fromString(throwFile.uuiDstart + "-0000-1000-8000-00805F9B34FB")


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private var isRunning: Boolean = false

    private var btSocket: BluetoothSocket? = null
    private var outStream: OutputStream? = null


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    init {
        // daemon-thread setting
        this.isDaemon = true
        // determining the default priority for the thread
        this.priority = NORM_PRIORITY
    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    override fun activate() {
        log.info("Searching catching device...")
        BtDeviceResearchThread(this, THROW_UUID).start()
    }

    override fun deactivate() {
        interrupt()
    }

    override fun getTimeID(): Date {
        return throwFile.timeID
    }


//======================================================================================================================
// IBtDeviceFinder
//======================================================================================================================

    override fun onDeviceFound(btSocket: BluetoothSocket) {
        this.btSocket = btSocket
        log.info("... catching device was found")

        start()
    }

//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    override fun run() {
        log.info("thread started")
        isRunning = true

        throwFile()
    }


    override fun interrupt() {
        isRunning = false

        outStream?.close()
        btSocket?.close()

        log.info("thread finished")
        super.interrupt()
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private fun throwFile() {
        log.info("BT-Throw process started")

        try {
            outStream = btSocket?.outputStream

            val fileInStream = FileInputStream(throwFile.path)

            // send file in pieces (CHUNKS)
            var bytesLeft = throwFile.fileSize
            val chunkBuffer = ByteArray(CHUNK_SIZE)

            while(bytesLeft > CHUNK_SIZE) {
                fileInStream.read(chunkBuffer)
                outStream?.write(chunkBuffer, 0, chunkBuffer.size)

                bytesLeft -= CHUNK_SIZE
            }

            // send file reminder
            val remainderBuffer = ByteArray(bytesLeft.toInt())
            fileInStream.read(remainderBuffer)
            outStream?.write(remainderBuffer, 0, remainderBuffer.size)
            outStream?.flush()

            // close file stream
            fileInStream.close()

            log.info("BT-Throw process finished")

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


}
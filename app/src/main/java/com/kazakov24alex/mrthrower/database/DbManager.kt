package com.kazakov24alex.mrthrower.database

import android.content.ContentValues
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import com.kazakov24alex.mrthrower.MrThrowerApp
import com.kazakov24alex.mrthrower.utils.wrappers.ContactWrapper
import com.kazakov24alex.mrthrower.utils.wrappers.ThrowWrapper
import com.kazakov24alex.mrthrower.utils.wrappers.MessageWrapper
import com.kazakov24alex.mrthrower.utils.experts.WifiNetworkExpert
import library.codes.*
import library.database.Database
import library.database.tables.*
import library.database.views.DbViewMessage
import library.database.views.DbViewThrow
import library.objects.GreetingObject
import library.objects.MessageObject
import library.objects.ThrowFileObject
import library.wrappers.IpAddress
import library.wrappers.MD5
import library.wrappers.MacAddress
import java.util.*
import java.util.Date
import java.util.logging.Logger


val DATABASE_VERSION = 1

object DbManager : SQLiteOpenHelper(MrThrowerApp.instance, Database.DATABASE_NAME, null, DATABASE_VERSION) {

    val log = Logger.getLogger(DbManager::class.java.name)

    private lateinit var db: SQLiteDatabase


    override fun onCreate(database: SQLiteDatabase?) {
        if (database != null) db = database

        db.execSQL(DbTableOs.createTableQuery())
        db.execSQL(DbTableThrowType.createTableQuery())
        db.execSQL(DbTableThrowMode.createTableQuery())
        db.execSQL(DbTableThrowState.createTableQuery())
        db.execSQL(DbTableDirection.createTableQuery())

        fillTableOs()
        fillTableThrowType()
        fillTableThrowMode()
        fillTableThrowState()
        fillTableDirection()

        db.execSQL(DbTableContact.createTableQuery())
        db.execSQL(DbTableMessage.createTableQuery())
        db.execSQL(DbTableThrow.createTableQuery())

        db.execSQL(DbViewMessage.createTableQuery())
        db.execSQL(DbViewThrow.createTableQuery())

        log.info("The database is created")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    override fun onConfigure(database: SQLiteDatabase) {
        // foreign keys including
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            database.setForeignKeyConstraintsEnabled(true)
        } else {
            database.execSQL("PRAGMA foreign_keys=ON")
        }
    }

    fun openDatabase() {
        db = writableDatabase
        log.info("The database is opened")
    }

    fun closeDatabase() {
        db.close()
        log.info("The database is closed")
    }


// =============================================================================================
// FUNCTIONS OF INITIAL FILLING OF TABLES
// =============================================================================================

    private fun fillTableOs() {
        val queries = DbTableOs.fillQueries()
        for(query in queries)
            db.execSQL(query)
    }

    private fun fillTableThrowType() {
        val queries = DbTableThrowType.fillQueries()
        for(query in queries)
            db.execSQL(query)
    }

    private fun fillTableThrowMode() {
        val queries = DbTableThrowMode.fillQueries()
        for(query in queries)
            db.execSQL(query)
    }

    private fun fillTableThrowState() {
        val queries = DbTableThrowState.fillQueries()
        for(query in queries)
            db.execSQL(query)
    }

    private fun fillTableDirection() {
        val queries = DbTableDirection.fillQueries()
        for(query in queries)
            db.execSQL(query)
    }

    
// =============================================================================================
// FUNCTIONS OF RECORDING TO THE DATABASE
// =============================================================================================

    fun insertContact(contact: GreetingObject) {
        val values = ContentValues()
        values.put(DbTableContact.COL_MAC, contact.macAddress.bytes)
        values.put(DbTableContact.COL_NAME, contact.name)
        values.put(DbTableContact.COL_DEVICE, contact.device)
        values.put(DbTableContact.COL_OS, contact.os)

        // Insert the new row, returning the primary key value of the new row
        try{
            db.insertOrThrow(DbTableContact.TABLE_NAME, null, values)
        } catch (e: SQLiteConstraintException) {
            // UNIQUE constraint failed
            if(e.message!!.contains("UNIQUE constraint failed")) {
                updateContact(contact)
                return
            }

            log.warning("SQLiteConstraintException: " + e.message)
            e.printStackTrace()
        }
    }

    fun insertMessage(message: MessageObject, mode: Byte, state: Byte, direction: Byte) {
        val values = ContentValues()

        val contactId = findContactIdByMac(message.address)
        values.put(DbTableMessage.COL_CONTACT_ID, contactId)
        values.put(DbTableMessage.COL_MODE_ID, mode.toInt())
        values.put(DbTableMessage.COL_STATE_ID, state.toInt())
        values.put(DbTableMessage.COL_DIR_ID, direction.toInt())
        values.put(DbTableMessage.COL_TIME_ID, message.timeID.time)
        values.put(DbTableMessage.COL_TEXT, message.text)

        try{
            db.insertOrThrow(DbTableMessage.TABLE_NAME, null, values)
        } catch (e: SQLiteConstraintException) {
            log.warning("SQLiteConstraintException: " + e.message)
            e.printStackTrace()
        }
    }

    fun insertFileThrow(fileThrow: ThrowFileObject, mode: Byte, state: Byte, direction: Byte) {
        val values = ContentValues()

        val contactId = findContactIdByMac(fileThrow.address)
        values.put(DbTableThrow.COL_CONTACT_ID, contactId)
        values.put(DbTableThrow.COL_MODE_ID, mode)
        values.put(DbTableThrow.COL_TYPE_ID, ThrowTypeCodes.FILE)
        values.put(DbTableThrow.COL_STATE_ID, state)
        values.put(DbTableThrow.COL_DIR_ID, direction)
        values.put(DbTableThrow.COL_TIME_ID, fileThrow.timeID.time)
        values.put(DbTableThrow.COL_FILENAME, fileThrow.filename)
        values.put(DbTableThrow.COL_SIZE, fileThrow.fileSize)
        values.put(DbTableThrow.COL_MD5, fileThrow.mD5.bytes)
        values.put(DbTableThrow.COL_PATH, fileThrow.path)

        try{
            db.insertOrThrow(DbTableThrow.TABLE_NAME, null, values)
        } catch (e: SQLiteConstraintException) {
            log.warning("SQLiteConstraintException: " + e.message)
            e.printStackTrace()
        }
    }


// =============================================================================================
// FUNCTIONS OF SELECTING FROM THE DATABASE
// =============================================================================================

    fun selectAllContacts(): List<GreetingObject> {
        val contacts = selectContacts(null, null)
        return contacts
    }

    fun selectContactByMac(macAddress: MacAddress): GreetingObject {
        val contactId = findContactIdByMac(macAddress)
        val whereClause = DbTableContact.COL_ID + " = " + contactId

        val contacts = selectContacts(whereClause, null)
        return contacts[0]
    }

    fun selectMessagesByMac(macAddress: MacAddress): List<MessageWrapper> {
        val contactId = findContactIdByMac(macAddress)
        val whereClause = DbViewMessage.COL_MAC_ID + " = " + contactId

        return selectMessages(whereClause, null)
    }

    fun selectUnreadMessageByMac(macAddress: MacAddress): Int {
        val contactId = findContactIdByMac(macAddress)
        val whereClause = DbViewMessage.COL_MAC_ID + " = " + contactId +
                " AND " + DbViewMessage.COL_STATE_ID + " = " + ThrowStateCodes.STATE_PROCESS

        val unreadMessages = selectMessages(whereClause, null)
        return unreadMessages.size
    }

    fun selectThrowsByMac(macAddress: MacAddress): List<ThrowWrapper> {
        val contactId = findContactIdByMac(macAddress)
        val whereClause = DbViewThrow.COL_MAC_ID + " = " + contactId

        return selectThrows(whereClause, null)
    }

    fun selectThrow(macAddress: MacAddress, timeID: Date): ThrowWrapper {
        val contactId = findContactIdByMac(macAddress)
        val whereClause = DbViewThrow.COL_MAC_ID + " = " + contactId + " AND " +
                DbViewThrow.COL_TIME_ID + " = " + timeID.time

        return selectThrows(whereClause, null).get(0)
    }

    private fun findContactIdByMac(macAddress: MacAddress): Int? {
        val contactsList = selectContacts(null, null)
        for(contact in contactsList) {
            if(macAddress.compareTo(contact.macAddress) == 0) {
                return contact.getDbId()
            }
        }

        return null
    }

    private fun selectContacts(whereClause: String?, orderBy: String?) : List<ContactWrapper> {
        val contactsList = ArrayList<ContactWrapper>()

        val columns = arrayOf(
                DbTableContact.COL_ID, DbTableContact.COL_MAC,
                DbTableContact.COL_NAME, DbTableContact.COL_DEVICE,
                DbTableContact.COL_OS)

        val cursor = db.query(DbTableContact.TABLE_NAME, columns, whereClause, null, null, null, orderBy)
        cursor.moveToFirst()

        while(!cursor.isAfterLast) {
            val contact = GreetingObject(
                    MacAddress(cursor.getBlob(cursor.getColumnIndex(DbTableContact.COL_MAC))),
                    IpAddress(byteArrayOf(0,0,0,0)),
                    cursor.getString(cursor.getColumnIndex(DbTableContact.COL_NAME)),
                    cursor.getString(cursor.getColumnIndex(DbTableContact.COL_DEVICE)),
                    cursor.getInt(cursor.getColumnIndex(DbTableContact.COL_OS)).toByte())

            val contactWrapper = ContactWrapper(contact)
            contactWrapper.setDbID(cursor.getInt(cursor.getColumnIndex(DbTableContact.COL_ID)))

            contactsList.add(contactWrapper)
            cursor.moveToNext()
        }

        cursor.close()
        return contactsList
    }

    private fun selectMessages(whereClause: String?, orderBy: String?) : List<MessageWrapper> {
        val messageList = ArrayList<MessageWrapper>()

        val columns = arrayOf(
                DbViewMessage.COL_ID, DbViewMessage.COL_MAC,
                DbViewMessage.COL_MODE_ID, DbViewMessage.COL_MODE,
                DbViewMessage.COL_STATE_ID, DbViewMessage.COL_STATE,
                DbViewMessage.COL_DIR_ID, DbViewMessage.COL_DIR,
                DbViewMessage.COL_TIME_ID, DbViewMessage.COL_TEXT)

        val cursor = db.query(DbViewMessage.VIEW_NAME, columns, whereClause, null, null, null, orderBy)
        cursor.moveToFirst()

        while(!cursor.isAfterLast) {
            val message = MessageWrapper(
                    timeID = Date(cursor.getLong(cursor.getColumnIndex(DbViewMessage.COL_TIME_ID))),
                    text = cursor.getString(cursor.getColumnIndex(DbViewMessage.COL_TEXT)),
                    dir = cursor.getInt(cursor.getColumnIndex(DbViewMessage.COL_DIR_ID)).toByte(),
                    throwMode = cursor.getInt(cursor.getColumnIndex(DbViewMessage.COL_MODE_ID)).toByte())
            message.address = MacAddress(cursor.getBlob(cursor.getColumnIndex(DbViewMessage.COL_MAC)))
            message.setState(cursor.getInt(cursor.getColumnIndex(DbViewMessage.COL_STATE_ID)).toByte())

            messageList.add(message)
            cursor.moveToNext()
        }

        cursor.close()
        return messageList
    }

    private fun selectThrows(whereClause: String?, orderBy: String?) : List<ThrowWrapper> {
        val throwsList = ArrayList<ThrowWrapper>()

        val columns = arrayOf(
                DbViewThrow.COL_ID, DbViewThrow.COL_MAC,
                DbViewThrow.COL_MODE_ID, DbViewThrow.COL_MODE,
                DbViewThrow.COL_TYPE_ID, DbViewThrow.COL_TYPE,
                DbViewThrow.COL_STATE_ID, DbViewThrow.COL_STATE,
                DbViewThrow.COL_DIR_ID, DbViewThrow.COL_DIR,
                DbViewThrow.COL_TIME_ID, DbViewThrow.COL_FILENAME,
                DbViewThrow.COL_SIZE, DbViewThrow.COL_MD5,
                DbViewThrow.COL_PATH)

        val cursor = db.query(DbViewThrow.VIEW_NAME, columns, whereClause, null, null, null, orderBy)
        cursor.moveToFirst()

        while(!cursor.isAfterLast) {
            val throwFile = ThrowWrapper(
                    timeID = Date(cursor.getLong(cursor.getColumnIndex(DbViewThrow.COL_TIME_ID))),
                    filename = cursor.getString(cursor.getColumnIndex(DbViewThrow.COL_FILENAME)),
                    size = cursor.getInt(cursor.getColumnIndex(DbViewThrow.COL_SIZE)).toLong(),
                    md5 = MD5(cursor.getBlob(cursor.getColumnIndex(DbViewThrow.COL_MD5))),
                    dir = cursor.getInt(cursor.getColumnIndex(DbViewThrow.COL_DIR_ID)).toByte(),
                    throwMode = cursor.getInt(cursor.getColumnIndex(DbViewThrow.COL_MODE_ID)).toByte(),
                    throwType = cursor.getInt(cursor.getColumnIndex(DbViewThrow.COL_TYPE_ID)).toByte())
            throwFile.path = cursor.getString(cursor.getColumnIndex(DbViewThrow.COL_PATH))
            throwFile.address = MacAddress(cursor.getBlob(cursor.getColumnIndex(DbViewThrow.COL_MAC)))
            throwFile.setState(cursor.getInt(cursor.getColumnIndex(DbViewThrow.COL_STATE_ID)).toByte())

            throwsList.add(throwFile)
            cursor.moveToNext()
        }

        cursor.close()
        return throwsList
    }


// =============================================================================================
// FUNCTIONS OF UPDATING IN THE DATABASE
// =============================================================================================

    private fun updateContact(contact: GreetingObject) {
        val values = ContentValues()
        values.put(DbTableContact.COL_NAME, contact.name)
        values.put(DbTableContact.COL_DEVICE, contact.device)
        values.put(DbTableContact.COL_OS, contact.os)

        val contactId = findContactIdByMac(contact.macAddress)
        val whereClause = DbTableContact.COL_ID + " == " + contactId
        db.update(DbTableContact.TABLE_NAME, values, whereClause, null)
    }

    fun updateMessage(contactMac: MacAddress, timeID: Date, state: Byte) {
        val values = ContentValues()
        values.put(DbTableMessage.COL_STATE_ID, state)

        val contactId = findContactIdByMac(contactMac)
        val whereClause = DbTableMessage.COL_CONTACT_ID + " == " + contactId + " AND " +
                DbTableMessage.COL_TIME_ID + " == " + timeID.time
        db.update(DbTableMessage.TABLE_NAME, values, whereClause, null)
    }

    fun updateThrowState(contactMac: MacAddress, timeID: Date, state: Byte) {
        val values = ContentValues()
        values.put(DbTableThrow.COL_STATE_ID, state)

        val contactId = findContactIdByMac(contactMac)
        val whereClause = DbTableThrow.COL_CONTACT_ID + " == " + contactId + " AND " +
                DbTableThrow.COL_TIME_ID + " == " + timeID.time
        db.update(DbTableThrow.TABLE_NAME, values, whereClause, null)
    }

    fun updateThrowPath(contactMac: MacAddress, timeID: Date, path: String?) {
        val values = ContentValues()
        values.put(DbTableThrow.COL_PATH, path)

        val contactId = findContactIdByMac(contactMac)
        val whereClause = DbTableThrow.COL_CONTACT_ID + " == " + contactId + " AND " +
                DbTableThrow.COL_TIME_ID + " == " + timeID.time
        db.update(DbTableThrow.TABLE_NAME, values, whereClause, null)
    }




// =============================================================================================
// FUNCTIONS OF DELETING FROM THE DATABASE
// =============================================================================================

    fun deleteContact(macAddress: MacAddress) {
        val contactId = findContactIdByMac(macAddress)

        deleteAllMessages(macAddress)
        deleteAllThrows(macAddress)

        val whereClause = DbTableContact.COL_ID + " == " + contactId
        db.delete(DbTableContact.TABLE_NAME, whereClause, null)
    }

    fun deleteAllMessages(macAddress: MacAddress) {
        val contactId = findContactIdByMac(macAddress)

        val whereClause = DbTableMessage.COL_CONTACT_ID + " == " + contactId
        db.delete(DbTableMessage.TABLE_NAME, whereClause, null)
    }

    fun deleteAllThrows(macAddress: MacAddress) {
        val contactId = findContactIdByMac(macAddress)

        val whereClause = DbTableThrow.COL_CONTACT_ID + " == " + contactId
        db.delete(DbTableThrow.TABLE_NAME, whereClause, null)
    }

    fun deleteMessage(macAddress: MacAddress, timeID: Date) {
        val contactId = findContactIdByMac(macAddress)

        val whereClause = DbTableMessage.COL_CONTACT_ID + " == " + contactId +
                " AND " + DbTableMessage.COL_TIME_ID + " == " + timeID.time
        db.delete(DbTableMessage.TABLE_NAME, whereClause, null)
    }

    fun deleteThrow(macAddress: MacAddress, timeID: Date) {
        val contactId = findContactIdByMac(macAddress)

        val whereClause = DbTableThrow.COL_CONTACT_ID + " == " + contactId +
                " AND " + DbTableThrow.COL_TIME_ID + " == " + timeID.time
        db.delete(DbTableThrow.TABLE_NAME, whereClause, null)
    }




    // TODO temp
    fun test_fun() {
        val greeting1 = GreetingObject(
                MacAddress(byteArrayOf(1,1,1,1,1,1)),
                WifiNetworkExpert.getIpAddress(MrThrowerApp.instance),
                "TEST_NAME_1",
                "TEST_DEVICE_1",
                OsCodes.ANDROID
        )

        val greeting2 = GreetingObject(
                MacAddress(byteArrayOf(1,1,1,1,1,1)),
                WifiNetworkExpert.getIpAddress(MrThrowerApp.instance),
                "TEST_NAME_2",
                "TEST_DEVICE_2",
                OsCodes.ANDROID
        )

        insertContact(greeting1)
       //insertContact(greeting2)

        log.info("TEST_CONTACTS inserted")

        val message = MessageObject(Date(), "test text")
        message.address = greeting1.macAddress
        insertMessage(
                message,
                ThrowModeCodes.WIFI_NETWORK,
                ThrowStateCodes.STATE_FINISHED,
                DirectionCodes.INCOMING)

        val message2 = MessageWrapper(message.timeID, "new text", ThrowModeCodes.WIFI_NETWORK, DirectionCodes.INCOMING)
        message2.address = greeting1.macAddress
        message2.setState(ThrowStateCodes.STATE_DELETED)

        val md5 = MD5(byteArrayOf(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15))
        val fileThrow = ThrowFileObject(Date(), "testfile.mp3", 77777, md5)
        fileThrow.address = greeting1.macAddress

        insertFileThrow(fileThrow,
                DirectionCodes.INCOMING,
                ThrowModeCodes.WIFI_NETWORK,
                ThrowStateCodes.STATE_FINISHED)

        updateContact(greeting2)
        //updateMessage(message2)

       /* fileTW.setState(ThrowStateCodes.STATE_INTERRUPTED_ID)
        updateThrow(fileTW)
*/
        log.info("OPA")
    }

    fun getProfile(): GreetingObject {
        return GreetingObject(
                WifiNetworkExpert.getMacAddress(MrThrowerApp.instance),
                WifiNetworkExpert.getIpAddress(MrThrowerApp.instance),
                "ALEX PHONE", // TODO:
                WifiNetworkExpert.getDeviceName(),
                library.objects.GreetingObject.OS_CODE_ANDROID)
    }

}